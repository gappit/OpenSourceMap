# coding: utf-8

import sys
from pathlib import Path as path

if __name__=='__main__':


    tpl = '''
    {
    "version":1,
    "code":200, 
    "data": {
    "title": "",
    "tags": "",                                                                                                                                                  "srcurl": "",  
    "body":"%s",
    "related": [],
    "questions": []
    }}
    '''
    
    prefix = sys.argv[1]
    assert prefix
    
    for file in path('./').iterdir():
        if file.name.startswith(prefix) and len(file.name) > len(prefix):
            print '#! ' + file.name
            with open(file.name, 'r') as f:
                tmp = (tpl % f.read())

                with open(file.name, 'w') as fw:
                    fw.write(tmp)
            print "OK-"
                
    
