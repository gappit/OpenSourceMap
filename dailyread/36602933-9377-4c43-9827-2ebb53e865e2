
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="http://nautidea-10007657.cos.myqcloud.com/nautidea.dailyread.logo.512.png">
    <title>《渔家傲》霍达, 来自日读，每日经典推送，珍贵时光我们与你相伴</title>
    <script>
    var _hmt = _hmt || [];
    (function() {
    var hm = document.createElement("script");
    hm.src = "//hm.baidu.com/hm.js?16f893f6ab4a9f06cd8ada9e8ccb5376";
    var s = document.getElementsByTagName("script")[0]; 
    s.parentNode.insertBefore(hm, s);
    })();
    </script>
    </head>
    <body bgcolor="#fdf5dc" style="line-height:200%;padding:20px;">
    <h3 id="渔家傲">渔家傲</h3>



<h4 id="霍达">霍达</h4>

<hr>

<p>海上仙山·历史老人</p>

<p>我现在是在什么地方？</p>

<p>打开中国地图，东经117度25分，北纬23度42分，在蔚蓝色海水中，漂浮着一个不规则的五角形小岛，像偶尔飘落的一片枫叶，像倒映水中的一颗星斗，像翩翩欲飞的一只蝴蝶。</p>

<p>当我踏上它的土地，才知道它很大，有194平方公里的面积，141公里的海岸线，人口16万。它叫东山岛，是福建第二大岛。从这里距香港210海里，距澎湖98海里，水上航线与东南亚各国四通八达。在中国打开窗口、面向世界的今天，得天独厚的地理位置使东山成为天之骄子、海上明珠。1985年2月，中央批准东山县为沿海经济开发区之一。</p>

<p>我从北京出发，时届初冬，香山红叶已经凋零。而东山迎接我的却是和煦的海风，温暖的沙滩，苍翠的丛林，斑斓的鲜花。登临纵目，看白帆点点，听涛声阵阵，我被海岛所陶醉；仿佛到了天之涯、海之角，和我长期生活的喧嚣的都市、纷扰的人间是那么遥远！想寻找什么语言表达自己的感受，首先涌人脑际的竟是《长恨歌》中的两句：“忽闻海上有仙山，山在虚无缥缈间！”</p>

<p>东山人，坦荡如大海，纯朴如泥土，诚挚如手足。和他们相处，你会觉得像清凉的海水淘尽一身风尘那样舒畅。</p>

<p>县委、县政府的领导班子正在开会，共商东山的改革开放大业。年轻的县委书记杨琼，大学生出身，身材瘦长，文质彬彬，举止稳重；县长廖进彩，年富力强，体态丰硕，温柔敦厚。他们指点江山，侃侃而谈，中心议题是“东山的经济开发如何起步”。东山附近海域是闽南渔场中心，年产各种鱼类达50万担，海产珍品如龙虾、对虾、石斑鱼、红鲟等等闻名遐迩，远销中外；东山拥有全国最大的硅砂矿，总储量在两亿吨以上，花岗岩、辉绿岩遍布全岛，更有漫山遍野的木材，取之不尽，用之不竭；东山风光旖旎，四季如春，是天然旅游胜地；东山地理独特，港口优越，是进出口贸易的理想枢纽……他们是身居“仙山”犹不满足的人，急切地向往着、创造着明天。</p>

<p>县图书馆馆长、县政协副主席陈汉波则更多地谈到东山的昨天和前天。他如数家珍地为我讲解东山的奇礁异石、人文古迹、风物掌故，俨然一部活的地方志，一位现身说法的历史老人。</p>

<p>东山奇，东山美，置身这“海上仙山”、“世外桃源”，你绝不会想到战争、灾难、饥饿、死亡这些不祥的字眼。不幸的是，东山的历史正是由这些字眼写成的！</p>

<p>历史老人那阅尽沧桑的双眼充盈了泪水……</p>

<p>绿色的梦</p>

<p>我当然不认为这是梦。</p>

<p>绿叶覆盖了我头顶的天幕；绿叶摩挲着我的手臂；绿叶遮住了我的视线。我在绿色的海洋中漫游，不知道哪里是边。</p>

<p>在浓绿深处，我看见一个人影，那么远，那么小，像随风飘落的一颗松子。我向他走近，走近，他突然不见了，消失得无影无踪，像一阵清风。在他刚才站着的地方，留下一双清晰的脚印。我惊异地注视着这脚印，又听见松子落地的声音，近在身旁。蓦然回首，我看见一张离得很近的脸，黧黑，清瘦，广额，耸颧，凸出的眉弓和一双微陷的眼睛。这是典型的闽南人的脸。我注视着他，他又突然不见了，融化在绿色之中。我想我也许是遇到了掌管绿色的神、树的精灵！</p>

<p>我走到哪里，就在哪里看到这张脸。</p>

<p>我遇到的每一个东山人，都熟悉这张脸。</p>

<p>他是谁？</p>

<p>“那是我们的老书记谷文昌啊……”</p>

<p>深情的呼唤伴随着一串串热泪。</p>

<p>谷文昌并不是闽南人。在解放战争的炮火中，他从太行山麓的林县出发，铁脚板走了好几千里路，1950年5月11日的夜晚才随着强渡海峡的木帆船登上了东山岛。那时，中华人民共和国宣告成立已有半年之久，最后的激战使国民党军队从这里溃退台湾。</p>

<p>谷文昌被任命为东山城关区第一任区委书记，当年10月又调任县委常委兼组织部长，不久提升为县长，又不久改任县委书记。</p>

<p>在战后的焦土上，谷文昌痛苦地徘徊。</p>

<p>啊，这就是东山吗？</p>

<p>没有绿色。大野茫茫，尘沙蔽日，仿佛塞外荒漠。外患内乱，兵匪横行，战火百年不息，撕碎了东山的绿装，万顷良田化为不毛之地。渔佬、农夫，穿的是“虎皮八卦衣”，吃的是番薯汤和野菜。十八岁的姑娘要出嫁了还没有裤子穿，从出生到嫁人，吃到的米还不足一斤。年关吃一顿干饭，把米汤作为邻里互赠的“礼品”。淡水贵如油，大人用麻绳绑住小孩，吊进井底去掏一点救命水。荒山无草木，捡拾牛粪当柴烧。</p>

<p>东山岛，荒岛，饿岛，死岛。仅1949年一年，就有2000多人死于天花，1340人沦为乞丐。与饿殍和乞丐并存的是1720家赌场、7家妓院和807家大烟馆。</p>

<p>这就是东山！</p>

<p>……</p>

<p>夜，谷文昌投宿农家，和老农蔡海福挑灯长谈。起风了，狂风呼啸，这间茅寮就像风雨飘摇的一叶小舟，随时都会被沙浪吞没。</p>

<p>“这里的风沙咋这么厉害？”</p>

<p>“这还不算大，大的能把房子整个埋了，牛顺着沙堆跑，踩塌了房顶漏进去摔死！听老人说，近百年间这一带有十三个村庄被风沙埋在地下，毁了多少良田、死了多少人哪，都被‘沙虎’吞了！那些流落外乡逃荒要饭的、下南洋谋生的，都是被‘沙虎’逼走的！”</p>

<p>海福揉着被长年的风沙打红的眼睛，谈“虎”色变。谷文昌触景生情，坐立不安。他看见了自己的父亲。父亲已经死了，为了砍柴把绳子系在腰间，没有荡过山涧就摔死了。他看见了自己的过去，十六岁的小石匠离乡背井闯山西，为的是讨一口饭吃……海福大叔，你是农民，我也是农民！农民不能没有土地。我们把东山的土地交给了你们，可这样的土地又有什么用啊？不治服“沙虎”，我这个共产党员还有什么脸吃人民的干饭！</p>

<p>漫天风沙，谷文昌带着一班人，沿着“沙虎”出没的踪迹，穷追不舍……</p>

<p>风沙漫天，谷文昌领着全县人挑土压沙、筑堤堵沙、种草固沙，肩膀被扁担压肿，脚踝被沙窝扭伤，也顾不上喘息……透过面前滚滚的黄沙，他已经看见天边的荒滩顷刻之间变成了一片绿洲！</p>

<p>不，海潮还在奔腾，刚刚筑起的堤坝立即冲垮了；风暴还在呼啸，刚刚压住的黄沙重新揭开了；烈日还在蒸烧，刚刚栽下的青草很快枯死了。大自然毁灭这一切简直容易得像儿戏，人的力量显得那么渺小！</p>

<p>谷文昌在风沙中奔跑，一处又一处，白白流逝的心血使他痛心疾首：为什么，为什么？这不毛之地难道容不下任何生命吗？</p>

<p>回答他的，是风沙的呼号和百姓的叹息。</p>

<p>一切如旧，灾荒和贫困笼罩着东山。</p>

<p>有亲戚在外的东山人心动了，学着前人的样子，卷起铺盖要离开故土下南洋。外国是天堂吗？那是拿性命去换钱，可是宁愿出去艰难混世也不愿被黄沙埋掉！不是他们不爱家乡，是东山的黄土不养人！</p>

<p>共产党人不是“治民”的官吏，他的天职是让人认识人的价值、人的力量，让人活得像人。</p>

<p>“不为民造福，还要我干什么？不治服风沙，就让风沙先把我埋掉！”谷文昌面对东山，指天为誓。他走上了一条以自己的生命作抵押的路。</p>

<p>自从踏上东山，就没打算离开，把自己和子孙后代都交给了东山。他从河南老家把儿女也接了来，给长子取名“豫闽”：“东山就是咱的家，是咱的第二故乡！”</p>

<p>踏荒途中，他站在岵嵝山东麓的武庙前沉思。这是一座有五百多年历史的古庙，依山而筑，石柱擎天，飞檐斗拱，木雕石刻，玲珑剔透，庙中供奉着三国名将关羽。谷文昌无心访古览胜，倒是庙前那几棵老态龙钟的古树吸引了他的视线。这是几棵刺桐树，历尽劫磨，饱经风霜，仍傲然挺立，肆虐的风沙并没有摧垮它。它使谷文昌如获至宝！他想：如果把东山大地到处都栽满了树……</p>

<p>他的眼前又出现了绿洲。他急不可待地返回县委，立即召集副书记陈维仪、县长樊生林和县委一班人研究植树治沙，十万火急！可惜这里没有一位林业专家助他一臂之力，那就学！谷文昌派人到广东去参观取经，又派了采购员外出购置物资、设备和树种。</p>

<p>一时间，东山县上上下下，人人都对植树着了迷。一位干部因公到厦门出差，以赤诚感动了鼓浪屿的园林管理员，亲手为他采摘了一串又一串的成熟的桉树籽。</p>

<p>树种，东山岛上奇缺的树种，东山人日夜盼望的树种啊，终于采来了，来自兄弟地区，甚至通过省林业厅和中央林业部辗转来自国外。谷文昌捧着这些珍贵的种子，像捧着一颗颗珍珠，分发到全县每个单位，指定专人培育，他自己则在沙滩开辟了二十亩苗圃，亲手试种。种子，撒遍了东山大地；政策，也随之播进心田：全县造林，国造国有，社造社有，队造队有，房前屋后植树归个人所有！让绿色快些染遍海岛！</p>

<p>“绿色的梦”，几番梦不成。</p>

<p>老鼠刺树栽下去，很快枯死了；林兜树栽下去，不久也死了；苦楝、沙竿栽下去，仍然死了……</p>

<p>厄运的恶性循环，使许多人心凉了。白埕大队索性一古脑儿把桉树、槐树、黄槿、相思树、合欢树和木麻黄都种下去，好歹就这一回了，活就活，死就死。</p>

<p>这批树一天天叶枯根烂，眼看就要死光，死光就不干了！</p>

<p>这时，林业干部吴志成气喘吁吁地从白埕赶来找谷文昌，谷文昌跨上吴志成的自行车后座，立即赶去……</p>

<p>白埕的沙丘旁，出现了一个足以使谷文昌发狂的奇迹：在厚厚的黄沙下面，钻出了嫩绿的生命！那是几棵澳大利亚木麻黄，骄傲地伸展着一簇簇丝线般的针叶，向谷文昌报告，它们活了！</p>

<p>“木麻黄，木麻黄，我可找到你了！”谷文昌半跪在沙滩上，伸出颤抖的手，动情地抚摸着，“多么顽强的树！它在这里能活，在别处也能活；能活几棵，就能活几百棵、几万棵！”</p>

<p>而木麻黄又和人们开了个不小的玩笑，种上不几天又死了，活的木麻黄竟然可遇不可求！</p>

<p>海福大叔在苗圃里搭起寮棚，谷文昌和他们一起日日夜夜试种、观察。终于，木麻黄之谜被揭开了，原来它虽耐旱，却又喜水，凡是雨天栽种的，两三天就生了根！啊，多亏了那场大雨，把最后的一道难关突破了，谷文昌高兴得手舞足蹈！</p>

<p>木麻黄，以它旺盛的生命力获取了第一个在东山落户的资格。县委向全县推广，从农村、工厂、机关、学校到营房、哨所、商店、旅馆，都建立了木麻黄苗圃，组织起造林专业队。在缺水的东山，人们蘸着汗水和雨水，把绿色的彩笔点染……</p>

<p>如今我来到东山，满眼都是葱翠的绿色。雄伟的木麻黄，挺立着笔直的主干，拂动着孔雀羽毛般的绿叶，一排排伸向远方，连成几十公里的“绿色长城”。走在公路上，两旁的树木扶摇相接，绿荫如盖，路面上几乎不漏阳光。木麻黄保持了水土，改变了气候，海风吹来，纤尘不起，给人带来了爽心的清凉和馨香，木麻黄捷足先登，日本银桦、法国梧桐、美国湿地松和当地枇杷、荔枝、柑橘、桃李、香蕉……也扎根东山，漫山遍野，绿树成荫，花果飘香。农田里，一碧万顷，稻浪翻滚，从往年的一年一熟变为一年三熟、四熟，亩产二三千斤，谁还会为填饱肚子发愁！赤山村，解放前是“乞丐村”，许多人被风沙逼得流落异乡，飘零海外。一个青年农民，当年家里没柴烧，把旧桌椅都烧了，最后只剩下一条扁担，老婆说：“别烧了，留着这点家当吧！”他扛着这条扁担告别了家乡。几十年后，他从新加坡叶落归根，仍然带着这条扁担寻找家门，东山却使他不认识了：这总不会是梦吧……</p>

<p>祖祖辈辈的梦，老书记谷文昌的绿色的梦，变成了现实。在东山的儿童听来，“沙虎”为患已经是遥远的历史传说，谷文昌这个名字则是妇孺皆知、有口皆碑的“伏虎”英雄。</p>

<p>啊，那个漫长而艰难的绿色的梦！</p>

<p>我在绿色的海洋中漫游，时时看见那飘忽闪现的身影，那张黧黑而清瘦的脸。</p>

<p>八尺门海堤·捧茶论英雄</p>

<p>东山人为筑起“绿色长城”，连续奋战了十几个春秋，取得关键性突破并初具规模是在1960年前后。</p>

<p>那是一个混乱的年代。高指标、瞎指挥、浮夸风像瘟疫一样在全国蔓延。东山县委面临着严峻的考验：十年建设海岛，十年取信于民，难道要自毁其业吗？头上是十二级飓风，面前是东山父老，何去何从？万不得已，他们只好“阳奉阴违”。上面让密植，县委一面硬着头皮向上级反映东山“土质特殊，不宜密植”，一面把密植的行动缩小到几块巴掌大的“试点”，应付差事。上面要“拔白旗”，县长樊生林踏遍全岛，却不忍拔一个。后来，他碰到一个比他还“顽固”的生产队长。</p>

<p>“你们这里修水库了吗？”</p>

<p>“没有。”</p>

<p>“为什么？”</p>

<p>“我们这里没有能修水库的地方。”</p>

<p>樊生林成了风箱里的老鼠，两头受气，上面压他，下面顶他，他火了：</p>

<p>“我……撤你的职，拔你的白旗！”说完，拂袖而去。</p>

<p>拔了这面“白旗”，他并没有打道回府，而是绕到山后，去察看地形，看看这里是不是可以修一座水库。山前山后转了一遭，他的心越来越沉重：那位生产队长说的完全是实情，县长冤枉他了！</p>

<p>第二天，樊生林找到这位队长，说：“你昨天说得对，我拔你的‘白旗’拔错了，今天来给你官复原职！”“朝令夕改”，改得好哇，让东山的老百姓在最困难的处境中也没有失去对党的信任。</p>

<p>然而，“反右倾”的滚滚浪潮毕竟不是他们这些人所能阻止的。1960年初，谷文昌和樊生林奉命去省里开会。这个会，是庐山会议精神的推广、泛滥，开得声势浩大、杀气腾腾。省委指示：每个县必须至少有一名常委以上的干部定为“右倾机会主义分子”。</p>

<p>会场的一隅，谷文昌和樊生林这一对老搭档面面相觑，窃窃私语：咱们东山揪谁啊？</p>

<p>整个东山的干部都装在他们心里。这些人，十年来，鞍前马后，追随左右。只要他们一声令下，就会冲锋陷阵，万死不辞。现在，要从其中点将了。点谁呢？好难啊，县委书记和县长愁眉双锁。十指连心，砍断哪个也不舍得！</p>

<p>“他们都不够资格！”樊生林低着头，一字一顿地说，“这些人都是做具体工作的，执行的是常委的决定，‘左’了，‘右’了，责任都在县委常委！”</p>

<p>谷文昌又何尝不这样想！“是啊，省里也让咱们在常委里揪一个，可是我找不着这个人啊！”</p>

<p>“不用找了，这个人不是你，就是我！”樊生林斩钉截铁地说。</p>

<p>两丁抽一，在劫难逃！</p>

<p>怀着铅块一样沉重的心绪，他们回到了县里。意想不到的“救兵”成全了樊生林……</p>

<p>半年前，庐山会议尚未开始，樊生林曾应驻东山部队之约去作了一次报告。他是个大老粗，报告没有讲稿，有啥说啥，和子弟兵拉家常，洋洋洒洒讲了好几个钟头。限于当时的历史条件和他的觉悟可能达到的程度，他把“大跃进”也歌颂了一番。然后，话锋一转，又讲了些令人挠头的事儿：大跃进好是好，可是把土地深挖好几米，番薯秧都烂在里头了，怎么能收好几万斤呢？大跃进好是好，可是让老百姓二十四小时连轴转，干完活还得学文化、搞赛诗会，睡觉的工夫都没有了，如此等等，他讲得忘乎所以，子弟兵听得热烈鼓掌。</p>

<p>时间过去了半年之久，樊生林早把这次“报告”忘在脑后，不料他当时讲的话却有人字字句句记录在案，如今乘大反“右倾”之机，樊生林的小辫子被揪到光天化日之下，连谷文昌也爱莫能助了。</p>

<p>被撤职的樊生林默默无言，他那颗悬着的心终于落在了实处：被“打倒”，一人垮台，全家遭殃，我明白。可是总不能把责任往下推，让下边的干部当替罪羊啊，那咱在群众的眼里成了什么东西？</p>

<p>县委书记谷文昌夜夜无寐，仿佛他的心在一滴滴流着鲜血。</p>

<p>夜沉沉，海茫茫。月亮带着巨大的风晕在阴云中穿行，时隐时现，云彩的黑影像魔怪似的在海面上闪过，伴随着呜咽的涛声。岸边的石崖上，“天下第一奇石”被海风吹得频频颤动——它已经颤动了千百年。“风动石”边，长眠着民族英雄黄道周的英灵。</p>

<p>面对历史的陈迹，谷文昌想得很多，很多。风动石使他枰然心动，黄道周慷慨就义前咬指，血书“纲常万古，义节千秋；天地知我，家人无忧”的壮举使他浮想联翩……</p>

<p>适逢此时，工程浩大的八尺门海堤即将破土动工，地委向东山要人。谷文昌主持县委常委会立即决定：把“右倾机会主义分子”樊生林派去！中国自古以来不都是这样的规矩吗？“罪臣”总是被罚以苦役，惟一的出路是“戴罪立功”！</p>

<p>八尺门，是东山岛和大陆之间的一道长180丈、深6丈、波涛滚滚的海峡。这里水深流急，暗礁密布，十分险恶，千百年来不知有多少渡人在此葬身鱼腹！</p>

<p>八尺门海峡沸腾了，马达轰鸣，灯火如昼，成千上万名尚未摆脱饥饿威胁的民工，又以和风沙搏斗那样的巨大热情向大海宣战。</p>

<p>工地上出现了一个奇怪的人，他的政治面目是“右倾机会主义分子”，而职务却是海堤工程总指挥。他中等身材，洗得发白的蓝制服上溅满了泥巴和海水；貌不惊人，苍白的瘦脸上被海风刻下了一道道皱纹。“这是我们的樊县长哇！”民工们向他投去崇敬的目光，仍然亲切地叫他“县长”。他在人民心中具有极高的威望、极大的号召力。</p>

<p>人民的信赖，使樊生林心灵深处的巨大伤痛得到了补偿。一名共产党员，除此之外他还需要什么呢？唉，早该筑这道大堤了，他想。要是早有这道大堤，解放东山岛时，我们也不至于牺牲那么多的同志了。都是因为海峡阻隔，我们的增援部队、支前船只遭受了那么大的损失，“八尺门大捷”付出了血的代价，东山烈士陵园里埋葬着累累忠骨！烈士的亡灵在注视着、期待着我们早日把大堤修好！</p>

<p>无须再记述修筑八尺门海堤的日日夜夜了，下面的两个数字足以说明一切：海堤工程原计划两年完成，结果只用了一年；原投资200万元，只用了150多万元，为国家节约了几十万元资金。</p>

<p>有人悄悄地问工程总指挥樊生林：“你当了‘右倾’还这么拼命？”他说：“定我‘右倾机会主义分子’，还没开除我的党籍嘛，党员到什么时候也不能偷懒，我……不会偷懒啊！”</p>

<p>比大海宽阔的是天空，比天空更宽阔的是人的胸怀。真正的共产党人不正是具有这样的胸怀吗？</p>

<p>我的这次采访，进出东山岛都要跨过八尺门海峡。自古天堑，已变通途，一道坦坦荡荡的长堤扼住大海的咽喉，把东山与大陆连为一体，将孤岛变为半岛，驱车而过，只在须臾之间。</p>

<p>伫立海岸，凝望长620米、宽17米、高16米的八尺门海堤，威武雄壮，气概非凡，令人肃然起敬。它，仅仅是一座堤吗？</p>

<p>今年，八尺门海堤刚满二十五岁，还多么年轻！而人生易老，昔日的总指挥樊生林年事已高，功成引退，在简朴的寓庐过着离休后的余生。</p>

<p>我到他家里去拜望这位历尽坎坷的老战士。他以清茶待客，慈眉善目，轻声慢语，和我拉着家常。他说话句子很短，几个字就要停顿一下，很响地咂一下嘴唇。说起当年的冤案，他只淡淡一笑：“党是了解我的，就在八尺门海堤落成之后不久，就给我甄别平反，重新安排了工作。我个人没受什么损失啊，倒是利用那段时间干成了一件实事。”</p>

<p>我喝着清茶，品味着他的话语。没有耿耿于怀的牢骚，没有居功自傲的炫耀，他说得多么轻松啊！英雄都不把自己看做英雄，这才是真正的英雄。其实，樊生林早在四十年前就已经是晋冀鲁豫杀敌英雄了，又何待今日评说！</p>

<p>滂沱大雨，为这颗虔诚的心做庄严的洗礼</p>

<p>遗憾的是，我已经不可能像拜访樊生林一样去看望他的老搭档谷文昌了，只能站在他那物是人非的故居，望着他的遗像作无声的交谈。</p>

<p>接待我的是谷文昌的遗孀史英萍——一位朴朴实实像家庭妇女似的南下老干部，从50年代直到离休一直只是行政十八级。因为每次提级，谁要提史英萍，谷文昌就首先反对。她样样听老谷的，丈夫的事业就是她的事业，除此之外，自己什么也没有了。</p>

<p>谷文昌行政十三级，已算“高干”，这样级别的干部在当地很少。如今闽南百姓富了，而这位“高干”的家却仍然寒酸得可以。他们子女多，住房只有四间，还专门辟出一间做接待干部和群众的“客厅”。家里除了必不可少的床铺之外，几乎看不到任何家具，只在客厅里他那幅遗像前摆着一对简易的木制沙发，还是他死后才做的。在他生前，家里一直用的是石桌、石凳。闽南的木材那么多，那么便宜，木器却是他的一大忌讳，避之若蛇蝎。这是为什么？</p>

<p>史英萍怀着深深的爱，向我述说着这个秘密……</p>

<p>当东山全岛铺满了绿荫，谷文昌已是全省闻名的林业专家，1964年被上调到省里担任林业厅厅长，统领全省造林大军。“文革”中经历了七斗八斗又回到东山所在的龙溪地区，先后担任林业局长、农林水利办公室主任和副专员。宦海沉浮，去而复回，两袖清风，伴随他辗转奔波的惟一家当是秘书替他买的一只樟木箱。他大半生都花在种树上，却没有伸手为自己要一寸木材。儿女结婚，想做点家具，他严令不许：“我是管林业的，家里不准用木器！如果我做了一张桌子，下面就会做几十张、几百张；我犯小错误，下面就会犯大错误；我不犯错误，下面还在犯错误哩！当领导的得先把自己的手洗净，把自己的腰杆挺直！”</p>

<p>就是这样，他成年累月、没日没夜地坐在粗糙的石凳上读书、吃饭，和络绎不绝的来访者交谈着没完没了的话题。史英萍心疼他，总想替他挡驾：“当领导的也得吃饭、睡觉啊，这么下去，你还活不活？”谷文昌对老伴说：“咱也是农民嘛，应该知道，农民来见‘官’，可不像走亲戚串门子那样随便，是想了又想、鼓足了勇气才来的。要是不让进门，他下次还敢来吗？出去还要骂，骂的就不是我谷文昌一个人喽！你想过没有？”老伴没法再拦他，眼看他饥一顿、饱一顿、冷一口、热一口，心被群众的疑难苦痛扯住，把吃饭都忘了。“也许，老谷的胃就是这样弄坏的……”史英萍说到这里，从肺腑中发出一声痛惜的长叹。</p>

<p>工作排满了谷文昌的生活日程，病魔悄悄地侵人了他充满活力的肌体。1979年10月的一天，他去广州参加秋季广交会，偶然发现胃发生了“故障”，第一次觉得咽不下东西，意识到有些不妙。但回到漳州之后，又因为工作繁忙丢到了脑后。12月30日那天，在老伴央求下他去医院检査了。年头岁尾，x光照片给他们的家庭投下了可怕的阴影：谷文昌胃部的贲门有病变，怀疑是——癌！家人不安，领导也催促他立即去上海检查。谷文昌却不忍心丢下工作：“咱们当地医院还没确诊，干吗到上海去？路那么远，又得浪费国家好多钱！”谷文昌，曾经用自己的工资抚养了那么多的东山孤儿，有的一直供到大专毕业，他是那么慷慨；让他把几十块钱公款花在自己身上，却显得那么吝啬！也许是他耳目闭塞、孤陋寡闻，没听说过那些平常而又平常的事：有的人带着夫人、公子、小姐、儿媳、孙子，乘飞机、坐软卧，游山玩水跑遍全国从不掏腰包；有的人动用多少万元的国家资金、无偿使用建筑工人为自己营造别墅；有的人开着髙级卧车浩浩荡荡地出巡仅仅为了约几条小鱼解闷；有的人拿着盖有官印的公函堂而皇之地走私、盗卖国宝和国防物资，将“四化”建设急需的外汇塞入私囊！不，他什么都知道。但他心中崇高的信仰不会因为那些人的存在而动摇，他藐视那些人，藐视那些官职比他高的、贴着“共产党员”标签的人。他管不了他们，而党心、民心是不可侮的。权势，权术，可以谋取金钱和地位，却永远无法谋取党心、民心。水可载舟，也可覆舟！这才是最危险、最不可救药的“癌”！</p>

<p>谷文昌默默地和危害他肉体的癌、和侵蚀党风的“癌”较量。他在争取最后的时间，为时不多的余生，要交给党，而不属于自己！</p>

<p>他又一次抱病去省里开会，终于躺倒在福州。省委、地委强行送他到上海治疗，由最好的医生做了贲门癌切除手术。但这手术毕竟太晚了，癌细胞已经扩散……</p>

<p>1981年的1月，80年代的头一个春天，谷文昌躺在病床上，奄奄一息。窗外，正下着大雨，风声，雨声，雷声……噢，这正是栽树的好时候！“我要回东山，我要回东山……”他喃喃地说着这最后的心愿，担心再晚就回不去了。老伴和儿女围在他的床前，忍着眼泪，做出笑颜：“等你病好了，咱们一定回东山！”</p>

<p>东山人赶来了，越过海峡，带来了木麻黄防护林带那醉人的清风，带来了十六万颗滚烫的心，“谷书记，东山人想你啊！”</p>

<p>谷文昌那深陷的眼窝流下了泪水。不是欣慰，不是感激，他此时面对东山人，感到的却是愧疚！——他，难道还有什么愧对东山吗？他想到的是：十年树木，百年树人，这两件事他只做了一半。朱子周、黄鸿度、蔡永康……都是鞍前马后追随他十几年的干部，风沙中出没，泥水里奔走，在机关一起熬夜，下乡一起睡在农民、渔家的床上。初到东山，他不懂闽南话，由这些人当“翻译”，他当时还不会骑车，这些人替他当“自行车司机”。他没有多少文化，总结、报告、无数的公文，都偏劳他们了，多亏了这些知识分子！可是……唉！谷文昌的心隐隐作痛：我怎么只知道对他们使用！我在东山时，他们是干事，到我调离东山，他们还是干事。如今的干部要年轻化，他们年轻的时候已经过去了！我把他们耽误了，他们本来可以成为大材料！别，你们……别这么看着我，别再念叨我的什么好处，我……对不起你们！</p>

<p>又一张熟悉的脸浮现在谷文昌的脑际，那是在东山时的老搭档樊生林。当年“反右倾”那一幕悲剧，二十年后的今天，一切都清楚了。当时犯了错误的不是“右倾机会主义分子”，他们是“诤臣”哪。他们蒙受了巨大的冤屈，却使党清醒了。我谷文昌一直是“糊涂”着吗？不，当时我和老樊一样，心里也“明白”着呢，却没能去当一个小小的“诤臣”！跟着党去犯错误，再跟着党去总结教训，毕竟比“上疏廷诤”容易得多了，没有任何风险，可是我们坐的这艘大船却差点儿翻了！前事不忘，后事之师，可惜我已经没有以后的机会了！</p>

<p>肉体和心灵的双重病痛在折磨着这位濒临死亡的共产党员，滂沱大雨，为这颗虔诚的心做庄严的洗礼。</p>

<p>正在龙溪地区视察工作的中共福建省委书记项南1月29日晚回到漳州，连夜就要去看望他。听到消息，谷文昌垂危的病体突然出现了转机，灰暗的双眼又放射出光彩。他的老伴和儿女喜出望外，以为他又有希望了，史英萍请人转告项南同志：“老谷的病见好，你就明天再来吧！”痴情的亲人啊，你们怎么没有想到，他的“见好”是生命之火熄灭之前的最后一次闪耀，他心里有话要向党说啊！</p>

<p>当夜，东山人民的好儿子谷文昌闭上了双眼，一颗忧国忧民的心脏停止了跳动。</p>

<p>窗外，大雨滂沱……</p>

<p>海峡对岸，东山岛141公里的海岸线上，参天的木麻黄林带在狂风中呼号，在暴雨中饮泣！</p>

<p>他死后，龙溪地委做出决定：把他的骨灰撒在宝岛，实现他“埋骨东山”的遗愿。</p>

<p>他死后，史英萍在一周之内拆除了家中的电话，连同谷文昌的手枪、自行车（这辆车从来不许孩子们骑）一并上交党组织：“这是老谷交代的，活着因公使用，死后还给国家！”</p>

<p>他走了，走得洁白，走得洒脱，走得高风亮节。</p>

<p>……</p>

<p>我望着他的遗像：肤色黧黑，面庞清瘦，满头白发，一双永远深情地望着百姓的眼睛。</p>

<p>我望着他的卧室，大大小小的工作日记，密密麻麻堆满了床头空地，好像他昨天深夜还在上面记下工作要点，好像他一会儿还会回来从中查找民间的呼声。</p>

<p>兵家必争之地</p>

<p>茫茫林海，巍巍长堤，当然不是一两个英雄建成的，奇迹的创造者是人民。跟着谷书记造林，他们经历了十几年艰苦卓绝的奋战；跟着樊县长筑堤，他们吃的是自带的干粮，住的是自搭的寮棚，没有领取国家一分钱补贴！</p>

<p>东山的百姓为什么心甘情愿这样做，东山的干部为什么有如此高的权威啊？东山人说：这是因为党的政策在我们东山掌握得好、执行得好。</p>

<p>这不是一句空话、大话，其中包含着巨大的容量。我更深地理解它，是在访问“台胞接待站”之后。</p>

<p>接待站，城关南门海堤边的这座占地2000余平方米、总投资38万多元的建筑，是专为来自台湾的同胞修建的，是他们的“寻根站”、“会亲站”、“还家站”。多少天涯断肠人在这里找到了归宿，多少离散的骨肉在这里得到团聚！剧痛和狂喜、噩梦和现实，在这里交织……</p>

<p>1950年的春天，国民党兵败东山，退据台湾。他们撤退的前夕，在东山进行了最后的疯狂劫掠，大搜粮、大派款、大抓兵，把4200余名东山人胁迫上船，岛上三分之一的家庭被拆散，留下多少新婚少妇、白发爹娘、无依孤儿！</p>

<p>东山解放了。在枪毙血债累累的伪县长那天，老百姓吃干饭庆祝，朝着毛主席像叩头谢恩，纷纷上台诉苦伸冤，涕泪横流！而那些苦大仇深、悲痛欲绝的渔民、农民，往往同时又是撤往台湾的国民党军人家属，在他们心中，亲与仇、爱与恨、情与法、家与国，互相交错、互相扭结、互相矛盾，难分难解。这些人不是一个两个、三家五家，而是好几万，再加上儿女姻亲、姑表旧眷，盘根错节，蛛网纵横，涉及人员遍布全岛，难道都把他们推到敌人一边去吗？</p>

<p>一个独特而棘手的问题摆在刚刚执政的共产党人面前。</p>

<p>命运，像一个飘忽不定的魔影，笼罩在千万人的头顶。政策，像千斤巨石在东山县委负责同志的心中权衡。</p>

<p>谷文昌、樊生林……他们都是从战争的硝烟中杀出来的老兵，身上还残留着国民党军队的弹孔，胸中还燃烧着仇恨，但是，面对这些孤儿、寡妇和老人，又费踌躇，慢思量。</p>

<p>海滩上，月朦胧。渔家妇女把已在异乡做鬼的丈夫的旧布衫穿在竹竿上，向着对岸呼唤魂兮归来……</p>

<p>油灯下，意绵绵。年迈爹娘用颤抖的手在年夜饭桌上多摆一副碗筷，默念着未归游子的乳名……</p>

<p>他们在战争中失去了亲人。翻身，解放，他们虽然也和别人一样分得了房屋和土地，却没有得到和别人一样的、本来应该享有的幸福和团圆。他们的亲人是被强行抓走的，仅仅一个铜砵村就被抓走青壮男子142名，所剩惟有老弱妇孺，这些人难道有罪吗？她们已经遭受了双倍于常人的浩劫，心灵上留下了永难愈合的创伤，共产党人决不能再在伤口上撒盐，而应该——救灾！</p>

<p>中共东山县委迈出了审慎而又勇敢的一步，就此创造了一个前所未有的新名词，称他们为“兵灾家属”。对他们政治上不歧视，使其享有和一切公民同样的待遇；经济上，困难户给予救济，孤寡老人由国家供养。</p>

<p>一项德政，众望所归。那些心里藏着难言的隐痛，曾经用犹豫、惶恐和期待的目光望着执政者的“兵灾家属”们，最懂得“信任”二字有多重的分量！</p>

<p>信任，连结了民心。</p>

<p>信任，沟通了海峡。</p>

<p>……</p>

<p>东山台胞接待站的电话铃声又紧急地振响了。</p>

<p>站长拿起话筒，电话是从平潭台胞接待站打来的，那里刚刚来了一位驾船到平潭港避风的台湾渔民。他在当年那场“兵灾”中从东山去了台湾，丢下了高堂老母和两个弟弟，几十年音信隔绝。海上的风暴给了他天赐良机，怀着狂跳的心向着故土靠拢，由于某种原因又不能在此久留，但他多想见一见朝思暮想的亲人啊，哪怕只看一眼！</p>

<p>此刻，海岛上空，大雨如注，就像积蓄了多年的相思泪一发倾泻而出！站长手握话筒，陷人了沉思：这位台胞所提供的，除了几十年前的地名、人名，再无任何线索。岁月流逝，人事沧桑，当年的踪迹能不能找到啊？那位台胞在平潭只能作短暂的停留，难哪！站长仿佛看到了那隔绝的母子怀着渺茫的希望在互相渴念的眼睛。他毫不犹豫地对着话筒说：“我们一定竭尽全力！”</p>

<p>冒着倾盆大雨，接待站全体工作人员立即出发，从台胞提供的原始地址找起，遍访百十户人家，毫无着落。他们想：说不定这位台胞当初是“讨海人”？于是又分头奔赴几个渔业大队去寻查，依然无人知晓。在线索将要断绝的时候，一位老渔民提醒他们：东山解放初期，“讨海人”大都在“第一渔业大队”。工作人员又上路了，沿着这一点蛛丝马迹去寻找沧海一粟，几经周折，终于找到那位台胞梦魂牵绕的母亲，年逾古稀，仍然健在。今天正巧亲戚家操办儿女亲事，老阿婆兴致勃勃地出门赴宴去了。工作人员追到喜宴上，气喘吁吁地告诉她这一喜讯，老阿婆顾不上喝喜酒了，迫不及待地要去看儿子。站长派了两名工作人员和一位医生护送，汽车全速向平潭急驶而去……</p>

<p>现在，文学语言显得贫乏了，我们无法描述那母子相见、热泪交流的情景！</p>

<p>儿子说：“阿妈，这些年，让你们受苦了！我这次冒着风险回来，没带什么积蓄，身上这点钱，你老人家拿去修修旧房吧……”</p>

<p>母亲说：“儿啊，妈不苦！家里早就盖了新房，你两个弟弟都成了家，妈已经儿孙满堂了！”</p>

<p>儿子说：“阿妈，我在那边想你们啊！这是我带来的台湾相思树种，你把它种在……”</p>

<p>母亲说：“儿啊，咱们东山过去没有树，如今满眼都是树，家门口的相思树年年都挂满了红豆！”</p>

<p>惊异，感叹，震动！故乡的巨变，亲人的安泰，使游子恍若身在梦中。在“那边”，他也曾在夜深人静之时偷偷收听福建人民广播电台播送的故乡佳音，但又心怀惴惴，未敢深信。历来“一人为匪，九族连坐”，他猜想家里的亲人恐怕早已……但现在，安然无恙的母亲就在他面前，亲口告诉他梦想不到的现实，他还能不相信生身之母吗？</p>

<p>相见时难别亦难。匆匆一见，儿子又挥泪上船了，频频回首望家乡，“阿妈，我一定会设法回家的！”</p>

<p>我和同行的作家十余人一起来到东山台胞接待站。前两天，有一艘台湾渔船在海上出了故障，生死关头，他们向东山联络呼救，很快被搭救上岸，绝处逢生。台胞说：“多亏了祖国，不然我们就没命了！”</p>

<p>我到客房去看望他们。那位船长因为船只和生产遭受损失，忧心忡忡，而十几位船员却兴高采烈，庆幸因祸得福，踏上了神往已久的国土。在这里，他们从住宿、膳食、娱乐到交通车辆，都受到免费款待。厨师变着法儿地让他们遍尝闽南风味，接待人员带他们尽情游览名胜古迹，他们像回到了久别的家乡，巴不得船慢点修才好呢。</p>

<p>不用寒暄客套，我们彼此一听说对方来自“北京”和“台湾”，就像故友重逢，促膝攀谈，亲如家人。“哇！你们是北京来的？北京，了不起的大城市啊！可惜北京不靠海，要不然，我们下次到北京去看看多好啊！”那种毫无掩饰、毫不造作的赤诚仰慕之情，竟使我们这些北京人有些意外。而且，他们人人会讲普通话，虽然带点闽南味儿，却清晰、流利。从地理位置上讲，台湾比香港、广州离北京更远，基但和台胞交谈却比同香港、广州人更方便！</p>

<p>一位面孔黑黑的年轻船员告诉我，他的祖上是明朝时从东山调去驻守台湾的水兵，“重洋百里戌台湾，猛甲澎湖递换班”。后来就留居那里，繁衍了大片子孙，到他这一辈已说不清是多少代了，但祖祖辈辈都记着东山是他们的“祖家山”，这里有他们的“根”！年轻的朋友胸中回荡着源远流长的血液。他情不自禁地为我们唱了一首动人的歌：</p>

<p>我离开家乡，</p>

<p>随风来漂流。</p>

<p>为了养家，</p>

<p>从八岁出来奔走。</p>

<p>阿妈在家等我，</p>

<p>一天一天等白了头！</p>

<p>船儿在海上漂流，</p>

<p>无论我走到哪里，</p>

<p>总要回头……</p>

<p>这是一首我从未听过的歌，却又像早已熟记心中的歌，那么熟悉，那么亲切，我轻轻地击节而和，潸然泪下……</p>

<p>流连至晚，依依而别。第二天，我们已回到几十里外的县委招待所，想不到，刚刚结识的朋友又从台胞接待站远道前来看望。我们临时举行了一个毫无准备的联欢会，从街上买回柑橘、香蕉招待台胞，台胞却俨然以闽南人自居，用也是刚买来的当地佳果，殷勤相劝我们这些北国兄弟姐妹，一时主客难分。其实，这时真正的东道主是东山的县委书记杨琼，台胞也把他当做北京来的了，我们道出他的身份，“哇！”台胞大为惊异。他们只是台湾的普通渔民，船长就是所见到的顶大的“官”了，哪里想到能和祖家山的“县官”见面，而且又是如此和蔼可亲、礼下庶人！</p>

<p>其实，台胞还不知道，接待站的日日夜夜、事无巨细，都挂在杨琼的心上，每逢中秋、重阳、元旦、新春，他们都是在接待站和台胞共同度过的！</p>

<p>那天，杨琼一直陪他们谈到深夜。</p>

<p>在现任“县官”身上，我看到了他们的前任、前任的前任一脉相传的遗风。从谷文昌到杨琼，东山的领导人已经更迭了好几茬儿，但这条“取信于民”的金线却连绵不断，贯穿至今。这正是他们赢得人民的拥戴、得以施展胸中宏图、将荒岛变为仙山的奥秘所在。</p>

<p>面对台湾海峡，面对历代兵家必争之地东山，我想到了“得民心者得天下”这句并不新鲜的古训。真正的“兵家必争之地”是民心！50年代中共东山县委在历史转折关头做出的战略决策所产生的巨大影响和深远意义，在80年代看得更清楚了。那么，到90年代呢？更远的未来呢？</p>

<p>还是那位“历史老人”陈汉波，陪同我登上全岛的制高点——东山气象站。登高远望，沧海横流，风云激荡。大陆在西，台湾在东，遥相伸出深情的手臂。在这纵观天象、预测风云的处所，他对我说：为什么东山呈五角形的放射状？因为她是一朵正在开放的海花，旅居台湾和新加坡、马来西亚、泰国……的数万东山儿女就像纷飞的蜂蝶恋着故乡。他们不断远涉重洋前来寻根认祖、探亲访友、避风修船、求医治病、洽谈贸易、投资建设，宝岛的开发和腾飞已不仅仅是“我们”的事了。当年，郑和下西洋的宝船从这里驶向世界，今天，更加辽远壮阔的航程又在这里扬起风帆！作家同志，再过二十年，欢迎你再来看看东山，那时我也许已经不在了，但是……</p>

<p>我说：东山永在，历史老人永远不会闭上双眼！</p>

<p>（发表于1986年第5期《报告文学》。收入霍达报告文学集《万家忧乐》，人民文学出版社1991年出版；《霍达报告文学选》，江苏文艺出版社1995年出版）</p>    
    </body>
    </html>
    