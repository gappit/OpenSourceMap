
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="http://nautidea-10007657.cos.myqcloud.com/nautidea.dailyread.logo.512.png">
    <title>《欢乐行程》阿来, 来自日读，每日经典推送，珍贵时光我们与你相伴</title>
    <script>
    var _hmt = _hmt || [];
    (function() {
    var hm = document.createElement("script");
    hm.src = "//hm.baidu.com/hm.js?16f893f6ab4a9f06cd8ada9e8ccb5376";
    var s = document.getElementsByTagName("script")[0]; 
    s.parentNode.insertBefore(hm, s);
    })();
    </script>
    </head>
    <body bgcolor="#fdf5dc" style="line-height:200%;padding:20px;">
    <h3 id="欢乐行程">欢乐行程</h3>



<h4 id="阿来">阿来</h4>

<hr>

<p>一场雪就把萧索大地变成了天堂。</p>

<p>阳光照亮起伏的山峦，蜿蜒的河流，孤零的村庄和覆盖这一切的白雪。野鸽群在天空中往复飞翔，搅起一个巨大的欢快声音的漩涡，在春天里分群的鸽子聚集起来，这样不知疲倦，在清冽的空气中欢快飞翔。</p>

<p>这个鸽群翔集的村庄叫做机。机村在大渡河上游，群山到草原的过渡带上。河谷开阔，山脉低缓。</p>

<p>阳光照亮格拉的脸。格拉是个很野的孩子，村里人说是没有父亲调教的缘故。次多则是有父亲而且调教很好的典范。可是次多不快乐，格拉快乐。格拉那张脸平常污垢很多，十天半月才会洗上一次。要不是他喜欢打鸟，要不是打鸟时喜欢到泉水边上，十天半月也未必会洗上一次。有些鸟喜欢落在泉水边的湿土中，享受那份湿润与沁凉。格拉静静等待小鸟飞来，有时就会遇到前来背水的母亲，她放下水桶，说：“格拉，看你那张狗一样的脸。”顺手一下，就把儿子的头摁进那一凼洁净的水中。又搓，又揉，最后用十指做梳子，清除头发中的草屑与松罗。格拉吱哇乱叫，母亲就会开心地格格笑出声来。</p>

<p>母亲一把一把撩水从上往下洗他的脸。</p>

<p>格拉的脏脸会把一凼水洗变颜色。母子俩坐下来，听从石缝中淌出的水潺潺作响，把那些污水冲掉。母亲有时会哭：“十六岁我就把你生下来了。”然后她又会笑，“你的脸跟狗的脸一样，难怪我认不出谁是你父亲，你汪汪叫啊，格拉。”这张脸其实不像狗脸。额头宽阔，亮堂，下巴尖削，且日后会方正饱满。只是双眼细小，明亮，聪慧中有一点猎犬的狡黠。两颗犬齿那么雪白，醒目地獠出嘴唇。</p>

<p>母亲背上水，桶的底边靠在腰肢上。向前走动时，腰肢就好看地起伏。“来吧，”她对儿子说，“格拉，我们回家了。”格拉就是狗的意思。格拉是小名。格拉没有大名，因为没有父亲。</p>

<p>满屋子的亮光使格拉醒来，立即他就听到了鸽子飞翔的声音。他一醒母亲就知道他醒过来了，不是相依为命的人不会有这样的感觉。“你不能穿新鞋上路了，”她的声音从外屋传来，“下雪了。”她的声音显得那么兴高采烈，“你就系一条新腰带吧，红色的那条。”母亲又喊：“快点啊，次多都来了。”声音圆润清脆，像是姑娘的嗓音。这嗓音常常招人议论。但是依然是母亲的声音，像把阴暗的房子和时日照亮，仿佛镀上一层白银的雪光一样。</p>

<p>次多是一个大家庭的孩子，他家里有一些值钱的东西。解放前是中等境况，解放后就成了富裕的人家。这种家庭严谨，节俭。这种家庭出身的人往往精明强干。但次多的一切却和家里人相反。现在，次多像平时一样拉着架子车来了，那样忧郁，那样沉默。车上装一袋胡豆，胶皮轮子压过积雪咕咕作响。等格拉吃完东西，次多已经把他那一袋胡豆弄上车了。于是，两人上路了。</p>

<p>新雪那么光洁，那么明亮。平常老实巴交的次多沉静的忧郁的眼睛那么闪闪发光，平常紧闭的嘴微微张开，有点惊喜的样子。</p>

<p>鸽群仍在天上飞舞，要等阳光融化了积雪，它们才能降落到翻耕过的土地里找寻食物。但它们好像不为积雪是否来临所焦虑，那样子奋力地凌空飞舞，在天地间抛撒欢乐的音符。</p>

<p>“看哪，次多！”次多停下脚步，回过头去，看到大路上只有他们自己的脚印与车辙。村子早已退隐到起伏山峦的背后去了。</p>

<p>现在，他们感到了故乡村庄的偏僻，宁静，以及和整个世界相距是如此遥远。就是他们，两个乡村的孩子，拉着重载的架子车从村子里出来，去三十里外的镇子刷经寺。用胡豆去换大米。镇子矗立在草原边缘，经常被无遮拦的风打扫，因此是一个洁净的镇子。风使空气显得稀薄，甚至阳光也是一样。镇上有一家三百个座位的电影院，用铁皮制作火炉与烟囱的手工作坊，百货公司和公共澡堂等等。镇上的居民有半年没有菜吃。于是用大米换胡豆。本地产的胡豆煮过，加上盐、油、辣椒面可以送饭；干炒可以佐酒。机村邻近的村子每年都有人去换些大米，给病人吃，或是节假日期间一家人一起享用这种精细的食物。机村却没人去换。像次多家那样有势力的人喜欢谈论自尊，喜欢用自己的看法给别人的生活定下一种基调，除非你从来就像格拉母子一样在这种基调之外。从前，次多家的基调也是由别人给确定的。现在，次多的二叔做了村长。他们就开始为别人确立基调了。</p>

<p>这样好，他们说，这样不好。</p>

<p>这是好的东西，他们说，这东西好吃。于是你就吞咽这种东西。在那里，次多首当其冲。有这样的机村人在镇上看见换胡豆的人挨门逐户，东家三斤，西家一盆。镇上那些吃国家粮的人明明十分需要，却做出高傲的样子。他们就说了。我们机村人不要这样。</p>

<p>次多的爷爷是一个自尊的人。近来却被越来越坏的胃所折磨，几乎不能进食了。格拉母亲说：“去给你爷爷换点米，不然他要饿死了。我们也换一点过年。”次多回去说时，他们不答应。他是晚饭时说的。他爷爷后来就呻吟了两个夜晚。他们就同意了。</p>

<p>一只野兔从路中间跑过。看到人来就躲进了柳丛。它拼命把脑袋往雪里钻，柳树落尽了叶子，变得那么稀疏，它高高撅起的屁股就暴露无遗了。</p>

<p>“它以为它藏好了呢？”次多从腰带上拔出弹弓，攥紧一团雪。雪团准确地弹射在它的屁股上。</p>

<p>“吱哇！”兔子叫了，往柳林更深处窜去。格拉用手罩住嘴，立即，猎狗清脆的吠声响起来了。兔子无法在冬天的柳丝中掩藏行踪。它窜到哪里，哪里枝条上的雪就簌簌下落，纷纷扬扬。</p>

<p>次多笑了。</p>

<p>“你笑了。”格拉说。</p>

<p>次多又笑了一下，脸上肉又僵住了。</p>

<p>山谷越来越宽阔，山变得更加低矮。退到离大路和河流更为遥远的地方。四野寂静无声。格拉大声呼喊自己：“嗨——，格拉！”声音传开，没有回来。却听到次多说：“天天下雪就好了。”“你说话了，次多，”格拉高兴地说，“你还笑了。”次多想：是啊，我笑了，我说话了。而在那个大家庭里，长孙也和长子一样处于一种隐忍的地位。次多把糖给央宗妹妹。次多给弟弟西拉叠个小飞机。次多给加央妹妹……次多！说几句话，逗逗他们，叫他们不要哭了。怎么你也哭丧着脸，总不说话。脸上肉像死了一样，连笑也不会。你……你看……来了亲戚什么你也喊个人，笑一笑啊。</p>

<p>次多心里山清水碧，但确实不容易说笑出来了。</p>

<p>“次多，嘿！”“嗯。”“晚上我想你不会来呢？”“你叫我是要来的。”“真的？”“真的。”“你不嫌我和阿妈是人人都看不起的？”“不。我还怕你恨我们家呢。”前面是一段陡峭的上坡路。车子上去，又后退；上去，又后退。最后是格拉用肩膀顶一只轮子往前一圈半圈，用石头支住，再去顶另外一只轮子。</p>

<p>终于上了坡。两个孩子在雪地上仰天躺下了。</p>

<p>喘过气来后，格拉说：“我们真行。”次多又笑了。</p>

<p>路上经过几个村子。遇到的成人都给他们以很高的礼遇，那就是和他们像面对大人一样地交谈、问候。他们说：看哪，天一下雪心里就好过一些了。只有一些和他俩年纪相差无几的孩子们向他们投掷雪团，高声叫骂来使嘴巴舒服。他们还唆使狗，跟在后面凶狠地唁唁吠叫。</p>

<p>起先，雪地里没有石头，他们就拉着车飞跑。跑啊，跑啊。狗却越追越凶，吠叫得更加疯狂。突然，格拉停住了，转身也愤怒地对着狗凶狠地吠叫起来。车子仍然带着次多前冲，听见原先三只狗的叫声变成了四只，四只狗的叫声混合在一起，然后就悄没声息了。他好像已经看到了：一个孩子被狗撕扯，殷红的血在他眼前的地上飞洒，更多的汗水从背心流下来了。</p>

<p>等他停住脚回头，却看到三只狗在雪地上欢蹦跳跃，绕着躺在地上的格拉。格拉对天汪汪吠叫，它们也一样汪汪地吠叫。格拉腾身而起，随便把一大捧雪撒向天空。狗们就趴下了，对他晃动尾巴。格拉含住手指。打一个长的唿哨，狗们就掉转头奔回它们的村子去了。</p>

<p>又是一段陡峭的上坡路。这次，挣扎许久，把好大一片雪踏成了泥泞，他们还是在原来的地方。后来是分成两次才把胡豆拖上坡去，擦去满脸汗水，才问：“先就怎么没有想到呢？”然后就放声大笑了。</p>

<p>这次，两人是同时开始笑的。只是次多笑得很沉静，格拉笑着笑着就躺在了地上。格拉把脸埋进雪里，抬头时就留下一张脸在雪地里。他说：“次多，看我雪中的脸，跟水中的不一样啊。你也来留一个吧。”次多就趴下，把脸平平地印向雪地，格拉还在他后脑勺上加把劲，按了一按。</p>

<p>一张宽脸，一张窄脸就留在了雪地上，轮廓光滑清晰。只有眼睛模糊不清，因而显得忧伤迷茫。</p>

<p>“给他们安上一对宝石眼睛。”“珊瑚就可以了。”“那样的眼睛看得见吗。”“算了，那样就成了菩萨像了。”那两张脸嘴巴是笑的。</p>

<p>当他们从那两张脸上抬起眼睛，远处镇子像一堆不规则堆积的雪撞入眼帘。</p>

<p>“刷经寺，”格拉叫道，“我们要到馆子里吃好吃的东西了。”“你有钱？”“阿妈给了我五块钱，以前是留下过年的，她说有了米过年就不要钱了。就把钱一张一张数给我了。”“我只有一个馍馍。我以为会给我一块钱的，他们有，你知道。”“算了。”格拉说，他看到次多忧郁的眼睛里备感孤独的神情。</p>

<p>“只有一个亲人，”次多说，“那样子才真好。”“我知道人家说阿妈话有多么难听，可我爱她。”平常，和母亲一样总是没有来由就高高兴兴，被人说成是一种疯癫的格拉。现在他一声不响了，弓下身子拉车。身子很低，拖着脚步，脚尖推动一堆积雪，像犁破开泥土。雪从鞋帮上头进了鞋子，在脚背上融化，沁凉的水在脚下有种非常舒服的感觉。</p>

<p>到了进镇子的一段下坡路上。</p>

<p>这段路一直和镇上的大街连成一气。他俩奔跑起来，双脚踏起的雪花不断撞在脸上。车速越来越快。格拉飞身上了板车，手中挥舞拉边套的纤绳，喊：“驾！”先是红柳，后来就是带院落的房子往后滑动了。</p>

<p>次多更加拼命地飞跑。身后，伙计的笑声响起来了，笑声抛洒在闪闪发光的街道中央。</p>

<p>他们一直到镇子正中的小广场上才停下。</p>

<p>刷经寺镇比以往哪一次见到的都还要洁净美丽，连医院的病人都换上了干净的条纹服装。房檐上挂下一串串晶亮的水珠，满世界都是水珠溅落的声音。百货公司的楼层是唯一重建的水泥房子。融化的雪水在平顶上汇聚到一起，从漆成红色的落水管中跌落，那声音竟有一条小河奔泻般的效果。格拉和次多提着秤，在一家家屋檐下进出，称出去胡豆，称进来米。遇到干脆的人家就用盆啦碗啦大致量一下。单数门牌的给格拉，双数门牌的给次多。在落水的屋檐下穿进穿出，两人的头发和双肩都给打湿了。</p>

<p>格拉一头鬈发更加鬈曲，像是满脑袋顶着算盘珠子。</p>

<p>直头发更直的是次多，一绺头发垂在额头中央，像一只引水槽，头上汇聚的水从那里落在鼻尖上面。再落到胸前，衣襟也湿了好大一片。</p>

<p>在双数门牌，一个老太婆给他们一人一只和她一样皱皱巴巴的苹果。出了门，格拉说：“看看你的老太婆。”并晃动手中的苹果。次多一口就咬掉了一半。</p>

<p>在单数门牌，一个弹琴的女人叫他们在院子中央的井里打水。格拉不干，次多干了。次多打水时，弹琴的女人指指自己绣有花朵的鞋子说：“你看我这样的鞋子能出去打水吗？”“你肯定有其他的鞋子。”格拉说。“可我不想打。”她边说边在琴弦上捋出一串和滴落的檐雨一样明净的声音。“你又不是地主资本家，他们都被打倒了。”女人晃动脑袋笑了，这些连山里的藏族娃娃也晓得了，她哈哈大笑，惹得格拉也嘿嘿地笑了。</p>

<p>刚提着水进屋的次多也跟着傻笑。</p>

<p>女人擦掉泪水，说她喜欢次多那样纯朴的不狡猾的孩子。她问次多要什么东西。次多用眼睛问格拉。格拉用藏话说：“酒。”次多就用汉话说：“酒。”女人说：“孩子家怎么喝酒，你也并不老实。”一副很失望的样子。</p>

<p>“我带回去，爷爷病了。”于是，他们得到一瓶红色的葡萄酒。他们在街上摇晃这瓶宝石般的东西。</p>

<p>“中午有喝的啦！”“你要喝？”次多吃惊地问。</p>

<p>格拉笑了：“你不喝？”“我……不会。”“以前你还不会换胡豆呢。我这儿的钱只够买饭，买菜，现在有酒了，就喝！”不知道是不是一下雪，人人心里都显得好受一些了。这天他俩还得到好几本连环画，一个男人还给他们一支和真枪一样大小的木头冲锋枪。“我以前在宣传队跳舞用的，”那人说，“《洗衣歌》听过吗？就是那种舞，我演班长。”要是他们不赶紧点头说知道，那人像是就要又唱又跳了。《洗衣歌》《过雪山草地》《逛新城》，女儿吔，哎！等等我，嗯！看看拉萨新面貌，等等，等等。等到换完粮食，又得到一只油灯，可以通过小把手调节灯芯长短的那种，还有一副脱了胶面的乒乓球拍。</p>

<p>街面上也开始化雪了。格拉的破鞋子里灌满了水。两只破鞋子在街上，在斑斑驳驳的雪中像两只鸽子咕咕叫唤。</p>

<p>车轱辘在身后吱吱作响。</p>

<p>两个孩子把架子车和车上的大米停在饭馆门口。周围是满镇子的水声。镇子上弥漫着稀薄的水的味道。阳光也似乎变得稀薄了。</p>

<p>饭馆里空空荡荡，胖厨师坐在灶火前打盹，他头也不抬，说：“吃饭还早。”“我们，我们有五块钱。”他抬起头，看见是两个娃娃：“不是从家里偷来的吧。”“怎么会，”格拉说，“我们来换大米。我们还带了酒呢？”“粮票呢？”“没有，我们那么多米，换你饭不行吗？”厨师想想：“一斤给我一毛柴火钱。”“好吧。”格拉大大咧咧地说。</p>

<p>“好吧，”厨师说，“看你（格拉）的牙齿，你（次多）的眼睛就知道你们都是诚实的孩子。过一个钟头来，车子我看着。”离开的时候，厨师还在唠叨：“可要早点回家，夜里上了冻，什么东西都要邦邦硬了。你们阿妈肯定不要你们邦邦硬躺在路上。”格拉捂住嘴笑：“嘻……嘻嘻。”“这有什么好笑。”“你从牙齿能看谁诚实还是不诚实。”次多仰头想，使劲想，也想不出来这有什么好笑：“你的牙齿比雪还白。”格拉更是笑个不停。</p>

<p>进了百货公司，格拉仍然在笑。对宽大的镜子和所有能映出面孔的崭新晶亮的器皿做着鬼脸笑。弄得次多不断伸手牵扯他的衣角。</p>

<p>他们开始花钱了。</p>

<p>次多在文具柜台前站住了。隔着玻璃是一柜子乐器，中间一大盒紫色的竹笛。次多的腰就弯下去，鼻尖一直碰到玻璃上。高悬的荧光灯在头顶嗡嗡作响，那光芒非常类似于雪的光芒。紫竹笛在这种灯光下闪烁的已非人间的光亮。次多喜欢吹笛子，他熟悉各种乡间民歌的曲调。但他那支笛子已经开裂了。村里会做笛子的那个老人也已经死了。格拉就给次多买下了一支。用了一块三毛钱。因为他看到伙计眼中那支笛子闪闪发光。</p>

<p>次多说：“笛膜。”声音很小。格拉听到了，又为他买了笛膜和一束红色的丝线穗子。</p>

<p>“我记住，一块六毛了，我要还。”格拉用力拍拍次多的肩膀：“你的眼睛要漏水了，伙计。我阿妈说好人就是这个样子的。”“你阿妈真好，格拉。”格拉又捶比自己长得高大结实的次多一拳头。格拉于是豪兴大发，在下一个柜台前买了一个熏鱼罐头，一听番茄酱和一些水果糖，走到街上，他们一分钱也没有了。</p>

<p>在饭馆里，他们对胖厨师说：“明年再来吃你的饭吧。”厨师说：“今年要不要喝口热汤。”次多赶在前头说：“不要。”离开时，胖厨师用勺子敲得铁锅丁当丁当响。</p>

<p>路上的雪已经化尽，到处是明亮的水洼。每个水洼里都有一角天空，或是一片云彩。原来天空可以分开，也可以拼合拢来。</p>

<p>“要是碰到镇上的娃娃，跟不跟他们打上一架。次多？”“他们在学校里呢。”“我是说怎么碰不到这些兔崽子。”“我饿了。”</p>

<p>离开镇子不久，他们就找到一个干爽的地方。他们停下车，用石头支住轮子。坐下来开始午餐了。</p>

<p>他们先把罐头上的包装纸细心地剥下来。上面，将要入口的东西画得那么鲜艳漂亮，那么清新诱人。</p>

<p>装鱼的玻璃瓶用石头砸开。次多则用刀子戳装番茄酱的铁盒子。</p>

<p>格拉说：“酒。”次多就用牙撕去玻璃纸封，拔出软木塞子。</p>

<p>“先吃鱼。”次多立即就伸手抓鱼。</p>

<p>“嗨，不忙。洗手。吃好东西的时候我阿妈都要叫我洗手。吃完，她就可以叫我，格拉，我的小狗，舔舔沾在爪子上的油水。”两个人就在石缝中，树阴下找残雪搓手。吃完鱼，酒也干掉了一半。他们像大人一样对着瓶口喝，故意把瓶子举得很高，看阳光使酒产生新的色彩，听酒在瓶子里丁咣丁咣。酒的味道和鱼的味道都非常之好。好得来不及仔细品尝。</p>

<p>而番茄酱就不怎么样了。</p>

<p>那么漂亮的东西：蜂蜜一样黏稠，一样晶亮的东西，颜色那么可爱的东西，味道却那么怪诞。第一口他们就差点呕吐了。但终于舍不得吐掉，于是用酒冲服，像吞什么药物一样。酒和番茄酱一齐消灭干净。现在，红色的东西变成了发烫的东西，熨帖的东西，轻盈的东西，来到了手上，脸上，胸前。酒变成了泡沫，轻盈透明的、欢乐吟唱的成百上千只蜜蜂一样上升到头顶。要使脑袋膨大，使双脚离开地面，到空中飞翔。</p>

<p>这样的感觉驱使他们倒退着走到大路中央，路面很奇怪地倾斜，他俩很奇怪站在这样倾斜的地上还这样稳稳当当。化雪后出来寻食的鸟在他们周围起落，飞翔，鸣叫。他俩掰碎手中的馍馍抛撒给鸟们，因而招来更多的鸟在他们四周起落飞翔。平生，他们第一次如此不珍惜粮食。鸟群因此歌唱。麻雀，百灵，画眉，还有羽毛黑白相间的点水雀，鸟翅扑噜噜响。</p>

<p>他俩掏出弹弓，瞄准罐头盒，酒瓶，射出一颗又一颗石子。玻璃碎屑飞溅，马口铁丁丁当当响。</p>

<p>“吹一下新笛子。”次多就给新笛子挂上红色的丝线穗子，给笛子上膜，并告诉格拉，笛膜是从芦苇中掏出来的。格拉问那么什么是芦苇，你见过吗？次多说我和你一样，但书上说它长在大水边，是像竹子的草。</p>

<p>于是，格拉说：“聪明的伙计上车吹吧。”自己拉起车子往前走了。次多绝对相当地聪明，不识谱也没有谱。抬手就吹出当时流行四方的歌曲。先是电影《农奴》插曲。后是《北京的金山上》。笛声一路在化雪后变得滋润的山野间飘荡。将要入冬的山野竟有了初春时的那种气息。那样地明朗清爽。融雪水甚至把有些封冻的河面上的冻重新破开，露出一汪汪平静的绿水。白桦，红柳沙棘带着一簇簇黄色果子倒映其中，美丽，静谧，那么地接近天空。</p>

<p>次多又吹起一支新的曲子，收音机和有线广播里常播的《牧民新歌》。这是在下坡路上，一段两三里长的下坡路。曲子的前奏却那么舒缓。格拉想放慢脚步，以适应笛子的节奏。但是不行。脑子在膨大，要提着双脚飘离地面。</p>

<p>车子在后面飞驰。</p>

<p>笛声也开始模仿群马飞奔的急促声音了。优美的笛声是多么流畅啊！</p>

<p>车子越来越快。</p>

<p>人飞起来，车子也飞起来，离开路面冲向了河边。</p>

<p>两个孩子腾身而起，尖叫着，比车子飞得更高更慢。他俩得以看到米口袋落在冰上，车子继续前冲，带着七零八碎的东西沉入了河水中央。然后，他们才摔在了沙滩上面。</p>

<p>两人都晕过去了一小会儿。但又很快醒过来，居然一点没有受伤。他们几乎同时抬起头来，吐掉啃了满嘴的沙子，呆呆地望着对方。米从摔破的口袋里漏到冰上，又从倾斜的冰面流到河里，刷刷作响。</p>

<p>“我死了吗？”“没死，你飞起来了。我死了吗？”“没死，你也飞起来了。”两个人大笑起来。米继续流进河里，那些连环画，木头枪，漂在深潭中央，被一个小小的漩涡慢慢依次吸附到冰层下面去了。那下面，还有他们的车子。</p>

<p>所有这些，他俩——格拉和次多——都忘记了。</p>

<p>“笛子，”次多问，“笛子呢？”“笛子呢？”格拉又问。</p>

<p>两人就在沙滩上狗一样爬着到处寻找笛子。到后来却发现，笛子依然紧握在次多自己的手上。</p>

<p>这次，两个孩子笑得更厉害了，一直把眼泪笑了出来。</p>    
    </body>
    </html>
    