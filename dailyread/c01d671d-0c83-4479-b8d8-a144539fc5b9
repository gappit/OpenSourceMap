
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="http://nautidea-10007657.cos.myqcloud.com/nautidea.dailyread.logo.512.png">
    <title>《站在无人的风口》陈染, 来自日读，每日经典推送，珍贵时光我们与你相伴</title>
    <script>
    var _hmt = _hmt || [];
    (function() {
    var hm = document.createElement("script");
    hm.src = "//hm.baidu.com/hm.js?16f893f6ab4a9f06cd8ada9e8ccb5376";
    var s = document.getElementsByTagName("script")[0]; 
    s.parentNode.insertBefore(hm, s);
    })();
    </script>
    </head>
    <body bgcolor="#fdf5dc" style="line-height:200%;padding:20px;">
    <h3 id="站在无人的风口">站在无人的风口</h3>



<h4 id="陈染">陈染</h4>

<hr>

<p>我第一次接触古老悠远的“玫瑰之战”，与我在十三年之后从某种高处，从心事重重的玫瑰丛里所怀的感悟大相径庭。我站立在无人的风口，瞭望到远古年代的那丛玫瑰仿佛穿越流逝的时光，依然矗立在今天。虽然已是风烛残年，但它永无尽期。我从历经数百年的它的身上，读懂了世界悲剧性的结构，我看到漫长无际的心灵的黑夜。</p>

<p>许多年以来，我一直想为此写一部独一无二的书，但每每想到这部书稿只能是一本哑谜似的寓言，使人绞尽脑汁去猜透其中的含义，便情不自禁把那开了头的草稿连同一个懒腰一同丢到火炉里去。我只能从它的余烬里拣出一星枝蔓散淡的什么。它的暗示不通向任何别处，它只是它的自身。</p>

<p>十三年前我住在P市城南的一条曲曲弯弯的胡同尽头的一所废弃了的尼姑庵里。</p>

<p>那一天，惊讶而恐惧的阳光闪烁不安地徜徉在凸凹的细胡同路面上，那光辉的表情正是十六岁的我第一天迈进那所破败荒废的尼姑庵的心情。已近黄昏了，这表情正犹豫着向西褪尽，它慢慢吞吞来来回回穿梭在蓬满荒草败枝的小径之上，涂染在面庞黧黑的碎石乱土之上。我做出安然自若、心不在焉、毫无感伤的样子，伴随着黄昏时分一声仿佛从浓郁的老树上掉落下来的钟声，一同跌进了地势凹陷于路面很多的庵堂的庭院。</p>

<p>尽管我做了充分的思想准备，我仍然对我所要暂时住宿的新地方怀有一种期待。我以为它会是像我在许多中国古老的寺庙绘画上见到的那个样子：庵门温和恬静地半掩着，里边有银子般闪闪发亮的大理石台阶，有泛着浓郁木香的高高阔阔的殿堂，有珍贵的金器，乌亮的陶器和老朽漆黑的雕木。然而，当我呼吸到庭院里的第一口气息之后，我便明白了我那微薄的梦想又是一场空。这里除了一股窒息凝滞的薰衣草气味和满眼苦痛而奇怪的浓绿，以及带着久远年代古人们口音的老树的婆娑声，还有四个硕大而空旷、老朽而破败的庵堂，余下什么全没有。</p>

<p>我警觉地睁大眼睛，生怕有什么动的抑或不动的东西被遗漏掉，担心在我意想不到的时候遭到它的惊吓或袭击。树木，衰草，残垣，锈铁，断桩，水凹以及和风、夕阳，我全都把它们一一牢记于心。</p>

<p>若干年以后，当我永远地离开了那个庵堂的庭院，无论什么时候想起它，我都记忆犹新。</p>

<p>一个对世界充满梦幻和奇异之想的十六岁女孩子，来到这里安身居住，绝不是由于我个人情感的毁灭，那完全是个人之外的一些原因。而我家庭的背景以及其他一些什么，我不想在此提及和披露。</p>

<p>事实是，我在这里住下来，住了四年半，我生命中最辉煌绚丽的四年半。</p>

<p>当我穿过庵堂的庭院东看西看的时候，忽然有一种异样感，它来自于埋伏在某一处窗口后面射向我的目光，那目光像一根苍白冰凉的手指戳在我的心口窝上。我沿着那股无形的戳动力方向探寻，我看到前院一级高台阶上边有一扇窄小肮脏的玻璃窗，窗子后边伫立着一个老女人或老男人的影像。实际上，我看到的只是一个光光亮亮的脑袋悬浮在伤痕累累、划道斑驳的窗子后边。</p>

<p>我是在第二眼断定那是个老女人的。她虽然光着头，但那头型光滑清秀，脸孔苍白柔细，很大的眼孔和嘴巴被满脸的细细碎碎的纹络以及弥漫在脸颊上的诡秘气息所淹没。那神情如此强烈地震动我，使我触目惊心。所以，当我的眼睛与那触碰着我心口窝的凉飕飕的目光相遇的一瞬间，我立刻闪开了。</p>

<p>我定了定神，想再仔细地看一眼那脸孔，这时那窗子后边已经空了。我有了勇气，伫立不动凝视着那扇空窗子。慢慢我发现，那空窗子正替代它的主人散发一种表情，它在窃窃发笑，似乎在嘲弄它外边的纷乱的世界。</p>

<p>我逃跑似的疾速朝着后院西南角落属于我的那间小屋奔去。我走进家人为我安排好的临时住所，紧紧关闭上房门。这是一间湮没在西边与南边两个庵堂夹角的新式小房子，房子的天花板很低，墙壁斑驳，有几件旧家具，简单而干净。室内的幽寂、湿黯和一股古怪的香气忽然使我感到释然。在墙角洗脸架上方有一面布满划痕的镜子，我在它面前端坐下来。于是，那镜子便吃力挣扎着反映出我的容貌。我对它观望了一会儿，忽然哭起来，我看到一串亮亮闪闪的碎珍珠从一双很大的黑眼睛里潸然而下。十六岁的眼泪即使忧伤，也是一首美妙的歌。一天来我好像一直在期待这个时刻。我一边哭泣，一边在裂痕累累的镜子前从各个角度重新调理了我的全部生活，像个大人似的周全而理智。</p>

<p>我长长地沉睡了整整一个夜晚。这一夜，我的一部分大脑一直忙碌于新生活的设计与编排。第二天醒来时我发现，无数的梦境已把我来这所庵堂居住之前的全部岁月统统抹去了。</p>

<p>那是个多雨的季节，我正在一所中学读高中，我照常每天去学校上课，一日三餐全在学校食堂里用饭，吃得我瘦骨伶仃，像一枝缺乏光照和水分的纤细的麦穗在晚风里摇晃着大脑壳。那时候我是个极用功的女学生，带着一种受到伤害的仇恨心理，一天到晚凡是睁着眼睛的时候全念书，睡眠总是不足，而那些乏味枯燥的书本每每总是使我昏昏欲睡。于是，我发明了一种读书法——边走边读。</p>

<p>每天傍晚时分，我从学校回到家就拿出书本到庭院里边走边读。晚霞总是染红西边庵堂顶部的天空，庭院里老树参差茂密，光线格外黯然，庵堂的大窗子像无数只黑洞洞的大眼睛盯着我缓缓走动。我非常喜欢这个远离喧闹人群的幽僻处所，我凭着身体而不是凭着思想知道，这地方从来就应该属于我。这里的幽暗、阴湿、静谧以及从每一扇庵堂的吱吱呀呀的沉重的木门里漫溢出来的阴森森的诡秘之气，都令我迷恋。</p>

<p>我每天进进出出的时候，都要对着前院高台阶上边的那扇窗子瞭望几眼，那里好像永远静无生息地酝酿什么，那个老女人只是静静地伫立窗前向我张望，目光含着一股凉飕飕的清澈。这种安谧与凝滞带给我一种无法预料的恐惧，我很害怕她有一天忽然冲我嘿嘿一乐。我始终对她怀有一种提防的渴望。</p>

<p>无论如何，有一束安静的目光伴随我进进出出，总能消解一些孤单。</p>

<p>许多年以来，我一直长久地怀念着那棵年代久远的老桐树。</p>

<p>正是夏季，有一天傍晚我照例在庭院里走来走去，慢慢默记着英国古代历史上那个著名的“玫瑰之战”事件。我一遍又一遍重复默念着一四五五年到一四八五年这个年代。兰加斯特家族与约克家族进行了一场由权位之争而引发的混战，前者的族徽为红玫瑰，后者的族徽为白玫瑰。在混战中两个家族互相残杀殆尽……我默记着从久远年代渗透过来的历史的血腥，默记着他们怎样一代一代变成残灰焦木，变成一逝不返的尘埃。我仿佛站立在一处通向历史与未来的风口，看到古老而辽阔的欧洲平原上，空漠苍凉的巷道里，人们厮打追杀的景观，一把把银光闪亮的兵器随着头颅一同落地，血像一簇红红的水沫，伴着洪荒时代的潮流走进历史，然后逐一淹没近代、现代和今天……</p>

<p>那个年代久远的历史事件本身，如今已无足轻重，但是从这时候起便有一个沉重的隐喻在我心头弥漫，尽管我当时并不懂得它。</p>

<p>院落里浓郁的老树伸手摊脚地摇荡着绿茵，小风柔和地在我身体与衣服的空间爬来爬去。我感到有些累了，就倚在那棵树冠蓬然、根部盘结收缠在土地之上的粗壮的老桐树上，感受着树叶们吵闹的静谧。</p>

<p>后来，我听到一阵轻微的叹息声，我警觉地四顾瞭望，周围什么全没有。我抬头仰视上空，如盖的浓阴微微颤抖，像一叠叠绿云在波浪，那种巨大而缓慢的蠕动，使人感到一种高深莫测的浮物正罩在头顶俟机降临。</p>

<p>接着，我又听到一声长长的气息，这一次听得格外清晰真切，似乎那凉飕飕的气息已经逼真地贴在我的后脖颈上边。我猛地转过身子并且向后闪了一步。</p>

<p>接下来是两个并行的场景：</p>

<p>A：身后依然什么全没有，想像的一切荡然无存。但那种空落和死寂使我觉得危机四伏，隐约感到有一双带寒气的眼睛正潜伏在已经糟朽了的庵堂圆木柱子后面，隐匿在后院与前院之间的那半截断壁残垣的夹缝里，悬浮在满院子的老杨树、珙桐和杉树们高高密密的茂叶上边，像无数探头探脑的星星趁着老树们闷闷地摇头摆尾之际，从浓密的树冠缝隙向下边觊觎……</p>

<p>B：我惊恐地转过身之后，看到一条白影像闪电一样立刻朝着与我相悖的方向飘然而去。确切地说，那白影只是一件乳白色的长衣在奔跑，衣服里边没有人，它自己划动着衣袖，掮撑着肩膀，鼓荡着胸背，向前院高台阶上边那间老女人的房间划动。门缝自动闪开，那乳白色的长衣顺顺当当溜进去……</p>

<p>我惊恐万状，努力命令自己清醒，告诉自己这肯定是一场梦。我挣扎了半天，终于清醒过来。应该说，是我的肩和手最先醒过来，它们感到一种轻轻的触压，凉飕飕的手指的触压，接着我的脑袋才醒过来，睁开眼睛。接下来我立刻被眼前的事情惊得一动不能动：前院高台阶上边那扇污浊不清的窗子后面的老女人正站立在我面前，她在向我微笑，我如此真实近逼地看到她的脸孔与身体：她的五官像木雕一样冷峻高贵，端庄的前额由于布满纹络，看上去如一面平展展的被微风吹皱的水湾的图案。光滑的头颅苍白得闪闪亮亮，她的眼睛黑漆漆凹陷进眼眶，有如两团沉郁的火焰，那眼睛仿佛是有声波的，随时可以说出话来。她的身体已经萎缩了，干瘪枯瘦，没有分量。</p>

<p>这个老女人第一次走进我的视野就用她的身体告诉我，这是一个靠回忆活着的人，今天的一切在她的眼睛中全不存在。</p>

<p>老女人的出现打断了我的关于恐怖场景的想像及编排，也打断了我许多天以来按部就班、从枯乏无味的书本上获得的那些关于玫瑰战争的记忆。</p>

<p>她动作迟缓地递给我一张图案，并且出了声。</p>

<p>“男人。”她说。</p>

<p>我熟悉这声音，沙哑、低柔，这声音仿佛是我自己的声音的前世。</p>

<p>我低头观望那幅图案，图案的底色是赭红色的，浓得有如风干的血浆，带着一股腥气。两把银灰色的木质高背扶手椅互相仇视地对立着，椅子上边是空的，没有人。</p>

<p>我说：</p>

<p>“男人?”</p>

<p>老女人说：</p>

<p>“两个。”</p>

<p>我两次低头观看那幅图案。</p>

<p>这一次我仿佛看出了什么，那两把高背扶手椅带着一种表情，它们硕大挺拔的身背散发出一股狰狞的气息；雕刻成圆弧状的敦实的木椅腿像两个格斗前微微弓起膝盖的斗士的壮腿，随时准备着出击；两个空落落的扶手正像两只冰冷的铁拳护卫在两侧，铁拳的四周弥漫着一股阴森森的杀气。</p>

<p>我不知道是否受了老女人那双会说话的眼睛的某种暗示，接收了什么神秘的气息传递，反正我忽然看出来那两只高背扶手椅的表情。</p>

<p>待我抬起头打算询问什么的时候，那老女人已经离开了。我的肩上还留有她的枯槁如柴的手指凉飕飕的余温。</p>

<p>天空慢慢黑下来。我回到自己的房间，闩上房门，拉上窗帘。窗帘是我这一生中最不能缺少的东西之一，我不能想像没有窗帘的生活。无论多么硕大多么窄小的空间，只要是我一个人独处，总不免习惯性地沉溺于无尽无休的内心活动，而我的眼睛和神态就会不由自主地出卖我，哪怕窗外只是一片空荡荡的没有灵性的漫漫长夜，哪怕只有低低絮语的游子般凄切的风声。</p>

<p>我把老女人丢在我手里的那幅图案漫不经心地斜倚在书桌与墙壁之间。洗漱一番之后，我便躺下来继续看书。</p>

<p>我的生活像一条小溪被人为地改变了渠道，但无论多么纤细渺小的溪流都会努力寻求一种新的惯性和归宿。我的生活完全湮没在读书这个惯性中。能够一个人独自呆着，就是我的归宿。</p>

<p>我继续玫瑰之战的默记。</p>

<p>兰加斯特家族即红玫瑰代表经济比较落后的北方大贵族的利益，约克家族即白玫瑰得到经济比较发达的南方新贵族的支持，最后约克家族从兰加斯特家族手中夺取了王位。世世代代连绵不息的争战与硝烟，使人民饱经灾难，人们自相残杀，社会经济完全耗尽。</p>

<p>对于战争的厌倦使我昏昏欲睡，我仿佛看到了笨重的木质战车坍塌在荒原之上，那残骸仍然在慢慢燃烧；断裂的轮胎仍在弥散出一股烧毁后焦糊的恶臭；一堆堆古老扭曲变形的锈铜烂铁重新排列成崭新的兵器，像一队队待命出征的士兵；骷髅们正在抖荡掉朽烂不堪的盔甲军衣，在夜空的一角慢慢从旷日持久、亘古绵长的沉睡中苏醒爬起……</p>

<p>我困得已经丧失了对任何历史事件合乎逻辑的记忆，便伸手熄了灯。</p>

<p>那时候的每一天，我那十六岁的睡眠都完整得没法说。可是，这一天夜半我却忽然惊醒，我看到斜倚在书桌与墙壁之间的那幅图案活起来。黑暗中，两把亮亮闪闪的银灰色高背扶手椅掷地有声地摇荡起来，沉沉闷闷的嘎吱嘎吱声越来越响，越响越快，似乎正在进行一场看不见的较量与格斗，那干枯的赭红底色慢慢溶化成流动的血浆。</p>

<p>我呼地坐起来，拉亮灯。一切重归于静寂，什么都消失。我以最快的速度用目光环视察看了那幅图案的前后左右以及房门窗口，一切安好如初。</p>

<p>我坐在床上呆呆地屏息不动。过了一会儿，我重新灭掉灯光。接下来的情景便证实了刚才所发生的一切的真实性——那两把银亮的高背扶手椅再一次嘎吱嘎吱摇荡起来。那铿锵有力的声音在低矮狭小的房子里四壁回荡，它们在一片赭红色的喧闹里古怪地挤来挤去，抢夺不息。</p>

<p>这一夜我在太阳一般橙黄色的灯光抚慰下警觉地和衣而眠。我不断地惊醒，房间弥散的昏黄的光亮有如一层薄薄的带纱眼的网罩，这网罩吸住我的目光，总是引向那斑驳的墙壁与油漆剥损的书桌之间，我便本能地在那地方努力搜寻发掘什么，再一次回味体验高背扶手椅骤然荡起的景观。我甚至想像起那一块血腥、暧昧、色情的赭红色背景上，那两把空荡的扶手椅所扮演的不共戴天的角色，在混战中他们脱下衣服投给他们共同的女人，他们巨大无比的身躯不需要互相碰撞就可以击倒对方。在僵持中不时有一张扶手椅猛然仰身翻倒，然后又迅速立起。他们不动声色的暗中撕扯与格斗使人难以分辨胜负。他们所争夺的女人在无休止的争战中默默地观望和等待，岁月在慢慢流逝，不知不觉中那女人春华已去，容颜衰尽，香消玉殒。</p>

<p>我在这孤孤单单、荒谬而奇异的境况中好不容易熬过这个没完没了不断惊醒的夜晚。这一个夜晚像一千个夜晚那么绵长无尽。夜间所发生的事情被我当时的正是夸张悲剧性格的年龄放大了一千倍，事情本身已走失了它的真旨原义，它成为我陷入对这个荒谬绝伦的世界的认识的第一步。</p>

<p>当东方的曙光轻轻地摸到我的窗棂的时候，我本以为这不可思议的一切都将结束。可是，接踵而来的事情不久便使我明白了我将进入另一个没完没了荒诞的夜晚。</p>

<p>清晨起床之后，我像归还一种命运一样立刻将那张两把扶手椅的魔画送还给前院的老女人。当时，老女人的房间寂然无声，我忽然失去了敲响她的门窗的勇气。于是，便把它轻轻放在通向老女人房间的高台阶上边。然后，我像往常一样去上学。</p>

<p>经过一夜的惊惧，我感到从脚跟不断向头顶弥散一阵阵眩晕。但是，鲜绿的清晨以及凉爽、澄澈的天气很快就洗涤了我身体的不适之感和头脑里的混沌迷乱。</p>

<p>我依然不喜欢校园生活的景观。晃眼的青灰色大楼，木然的白炽灯，消灭个性的大课堂，奔跑阳光的操场，都令我厌倦。在这儿，我只是众多的千篇一律的棋子中最不显眼的一只，我的浑身都活着，惟有我的头脑和心灵是死的。但是，我喜欢我的历史老师，这是一个学识渊博、善于借古说今的教师，任何一个已经死去的久远的年代，以及早已消亡殆尽的人物或事件，经过他的嘴就过滤得鲜活，仿佛就在跟前。他本人就是一个悠长的隧道，贯穿远古与未来。他从来不摆布“棋子”，而是注入“棋子”以思想和生命。可以说，我青少年时代的思想之门就是在历史课的叩击声中打开的。</p>

<p>那一天讲述的依然是玫瑰之战。</p>

<p>现在回忆起来，白玫瑰家族与红玫瑰家族血淋淋的战绩累累难数，但这些赫赫战绩的细枝末节经过数百年时光的沉淀，业已成为一堆不成形的点点滴滴，两败俱伤的结局以及王朝的覆灭都微不足提，它只给亘古如斯的岁月投下一瞥蜉蝣般的影子。留在我自己的记忆和历史的记忆中的只剩下争战之后的一片呜咽的废墟，悲凉的荒地。</p>

<p>这一课在我早年贫瘠的思想中注入了一滴醇醪，若干年之后我才感到它的发酵与膨胀。</p>

<p>傍晚我散了学回到庵堂的庭院。</p>

<p>高台阶上边的老女人从门缝探出她的光头，用苍白的手指招呼我。我停住脚犹豫着，然后鼓足勇气向她走过去。</p>

<p>老女人的房间灯光黯淡，闪烁着踌躇不安的光晕。破损的窗子上没有窗帘，无能为力地裸露着。我对于封闭感的强烈的需要，使我首先发现了这一点。这时候，裸窗于我非常适宜，我下意识地感到在这个神秘诡异的房间里，敞亮着的窗子会使我多一份安全。实际上，即使房门窗子四敞大开也无济于事。庭院里除了茂盛的老树们哀声叹息，什么人也没有。</p>

<p>月光从那扇光秃秃的窗子外斜射进来，洒在老女人苍白而泡肿的面庞上。我背倚着门窗，冷漠而惊惶地凝视着她的脸孔。她的脸孔阴郁、孤寂，蒙着一层甩不掉的噩梦。她的眼睛被无数皱纹拥挤得有些变形，闪烁着一种模棱两可的光芒。如果我忽略过这种变形，便可以看到这双眼睛在年轻的时候格外柔媚灿烂，她的脸颊也漾出白皙迷人的光华。</p>

<p>而此刻她的神情正在向我发散一种疲惫而衰弱的歉疚之色，我在一瞬间便抓住了这神色的背后她的孤独无援和渴望被分担。</p>

<p>她与我毫无共同之处，无论年龄、内心，还是外观。她春华已尽，衰老不堪，内心沧桑，而那时的我正清纯绚烂，充满梦幻。可是，她的神情顷刻间便改变了我原有的冷漠与惊惶，我那短暂的一瞥便使我完成了对于这个沧桑历尽的老女人的全部精神历程的窥探，使我蓦然对她泛起一股长久的怜悯之情。</p>

<p>应该说，她的那些拥挤叠摞的旧式家具是上好的，但它们毫不规则地胡乱摆放，以及覆盖在它们身上的积年的尘土渍迹和蜘蛛网，使人看上去她的房间零乱拥挤，破败不堪。房间里弥漫一股糟朽之气，仿佛是旧物商店里浮荡的那股霉腐味。那一张硕大的枣红色雕花硬木床夺去了房子很大的空间，这种床带有典型的中国旧时代遗风，床板很高很大，床头床尾挺括地矗立起花纹复杂的栏木，床板的上空有个篷子，有点像七十年代中国北方大地震时期人们自造的抗震床。那种气派、奢华散发一股帝王之气，但绝不舒适实用。</p>

<p>她的床上堆放着许多衣物。她的手在那堆零乱物上准确而熟练地摸到了什么，然后便把它们像陈旧的往事那样缓缓展开。我注意到那是两件我祖父年轻时代穿的那种锦缎大褂，一件是玫瑰白色，另一件是玫瑰红色。她枯瘦的手指将它们展开时的那种吃力和小心，仿佛是搬弄横陈的两具尸体，仿佛那尸体刚刚失去生命，它们身上的神经还没有完全死亡消散，如果用力触碰它们，它们仍然会本能地颤动。摆弄一番之后，两件长衣便冷冰冰地躺在床上了。</p>

<p>老女人说：</p>

<p>“男人。”</p>

<p>我想起了在庭院里那棵老桐树下她丢给我的那两把高背扶手椅图案。</p>

<p>我说：</p>

<p>“他们在哪儿?”</p>

<p>老女人看了看那两件红白长衣，说：</p>

<p>“两个。”</p>

<p>我说：</p>

<p>“他们两个都是你的男人?”</p>

<p>老女人点点头，然后又迟缓地摇摇头，不再出声。</p>

<p>许多年之后，我回想起老女人的时候，才发现她对我说过的话总共就这四个字。</p>

<p>当时，她不再出声。我便低头观望那两件并排而卧的长衣。我发现那两件长衣高高的领口正在缓慢扭动。一会儿工夫，两个没有头颅的空荡的颈部就扭转成互相对峙的角度，似乎仇视地在邀请对方决斗。</p>

<p>老女人抱起一件红色长衣，把它挎在臂弯处。然后，她开始脱自己的衣服。然后，我便看到了我极不愿去看然而还是抑制不住看到了的她那萎缩褶皱、孱弱无力、衰老朽尽的老女人的裸身。那干瘪的空空垂挂着的Rx房，那被昏黄的灯光涂染得像老黄瓜皮一样的胸壁，那松软而凹陷的腹部，我看到她那完全走了形的女人的身子感到一阵寒冷和恶心。</p>

<p>无论如何，我没办法把这样的身体称之为女人，然而她确确实实是女人。我无法说清这两者之间岁月所熬干榨走的是一个女人的什么，但我知道那不仅仅是一个女人的备受摧残的血肉之身。</p>

<p>当时我所想的只有一件事：我决不活到岁月把我榨取得像她那个样子，决不活到连我自己都不愿观望和触摸自己身体的那一天。</p>

<p>当我的头脑像生锈的机器来来回回在这一点上转不动的时候，老女人已经穿上了那件玫瑰红色长衣，宽大颀长的红衣立刻将她的身体和心灵完全吞没。她无比钟爱地抚摸着那光滑高贵的颜色，恣意而贪婪地露出她的欣喜之情。然后便脱下来，穿上另一件白玫瑰色长衣，那锦缎亮亮的白光反射到墙壁上晃得房间里四壁生辉。不知是否光芒的缘故，她的一颗干涩的老泪溢出眼眶，仿佛一颗熟过头的干瘪的荔枝在秋风里摇摇欲坠。</p>

<p>老女人表演完这一切之后，开始穿上自己的衣服。她的动作极缓极慢，仿佛要撑满整整一个漫漫长夜的寂寞。</p>

<p>我很渴望她能说些什么，但是她除了一连串的动作，无一句话再说。</p>

<p>墙壁上那只大半个世纪之前的挂钟，带着衰弱喑哑的气息敲响了，它响了整整十声。这绵延的钟声已经精疲力竭，仿佛拖着长音从数十年前一直摇荡到今天。当它那沉闷的最后一响敲过之后，奇异而令人震惊的事情便爆发出来。</p>

<p>那两件静无声息地瘫软在床上的红白长衣，猛然间像两条鲜艳的火苗疾速蹿起，它们撑住自己的身躯，犹如两个饱满慓悍的斗士向对方出击。最初，它们还保持着距离周旋，俟机伸出猛烈的一击，房间里不时响起“嗖嗖”的出击声。一会儿工夫，那两团光焰便扭抱在一起，红白更叠，纷纷扬扬，令人目不暇接，厮杀声也变得沉闷而铿锵。</p>

<p>这忽然而起的一切使我惊恐无比，魂飞魄散。在我打算转身逃离老女人这个溢满魔法的房间时，我一眼之间看到她期期艾艾忧忧戚戚坐在一旁观望、等待的木然的身躯。</p>

<p>这是我第一次走进她的房间，也是最后一次。这最后的一眼，使我读懂了她一生的空荡岁月。我看到一株鲜嫩艳丽的花朵在永久的沙漠里终于被干旱与酷热变得枯萎。</p>

<p>…………</p>

<p>我在那个与世隔绝、荒寞孤寂的废弃的尼姑庵生活了四年半。在这短暂而漫长的时光里，我有几次都怀着怜悯的心情想走进老女人的房间，我那与生俱来的对于自己的同类的苦难所怀有的同情与温情已在蠢蠢欲动，但终于每一次我都被她那永远捉摸不透的怪癖所引发的一种潜伏的危险感阻止住，放弃了对她的一点点抚慰。为此，我至今对她怀有一种深深的负罪感，仿佛我是吞没了她一生的那些苦痛与孤独的同谋。</p>

<p>我虽然再没有走进她的房间，但她的一生常常使我陷入一种茫然无告的沉思之中。她的那间诡秘阴暗的房子永远停留在我思维的边缘。我常常想，熬过了这么漫长的孤寂与心灵的磨难，她仍然能活着，真是一桩奇迹。</p>

<p>一直到我离开那所废弃的尼姑庵的时候，她仍然活着。现在回想起来那段孤寂而可怕的生活，我一点也不后悔我曾经有过的这段经历。当时，由于我的羞愧与自卑，我从没有引领着我的任何一个女同学男同学走进我的院落我的小屋。对他们也绝口不提我生活中的一点一滴。但是，现在我知道我是多么的富有，这种富有值他们一千个一万个。</p>

<p>老女人——尼姑庵里的那个老女人，在我离开那里之后的很长时间，我的思维总是看见她一动不动地靠在高台阶上边那个窗子前。她双目低垂，她的忧戚而衰竭的脸颊，苍白枯槁的手臂都已在静静的等待中死去，只有她的梦想还活着。她的身后，那两个奄奄一息的男人的长衣，仍然怒目而视，望着她正在慢慢僵死的胯部和身躯，无能为力。</p>

<p>十三年流逝过去。</p>

<p>现在，我坐在自己的一套宽敞而舒适的寓所里。我的膝头摊满白色的纸张，手里握着一枝黑色的笔，沉溺于对往事和历史的记忆。</p>

<p>这时，两个男人像幽灵一样走到我面前。惶恐之间我发现他们分别穿着我叙述它们厮杀在一起的那两件红、白长衣。他们是我的密友A君B君，这两个一向互相敌视的男人忽然之间协和起来，甚至互相丢了个眼色，然后一起动手，不容分说抢过我膝头上洒满文字的纸页，气咻咻叫嚷：什么时候我们的衣服厮杀起来过!我们从来也没有用高背扶手椅去对抗周旋!一派谎言!你编弄出这些香怜玉爱、格斗厮杀、血腥硝烟，你到底要说什么!</p>

<p>他们说一句便把我的稿纸撕几页，最后他们把我的故事全部撕毁了，地毯上一片白哗哗的纸屑纷纷扬扬，只留下尼姑庵前院的那个老女人伫立窗前的一段在我手里。</p>

<p>你是个残酷的女人，你永远清清楚楚。留着你手里的那一页吧，那是属于你的命运。</p>

<p>两个男人说完携手而去。</p>

<p>望着他们的背影，我看到若干年之后又将有人伫立在尼姑庵那扇窗子前向外边窥探。</p>

<p>我忽然想起来，那老女人的两个男人终生的格斗厮杀，最终使她没有成为一个真正的女人；我甚至想起来玫瑰之战中兰加斯特家族与约克家族数十年的争战，最终使王朝覆没。由于背景的缘故，这两个事件深处的内涵已经无法回避地在我的头脑中组结在一起。</p>

<p>一个女人就如同一个等待征服的大国。或者说，一个国家就如同一个女人……</p>

<p>一四五五年那个事件正在穿越无边的岁月，穿越荒原、火焰、潮水、余烬、洞岩、死亡以及时间的睡眠在蔓延。</p>

<p>我知道故事无疑重新开始叙述，不断开始。</p>

<p>只是，任何一种重复都使我厌倦。哪怕是有关一个国家、一个民族以及人类命运这样重大问题的叙述。</p>

<p>我伸了伸懒腰，把手里仅剩的那一页稿纸和那枝爱多嘴的黑笔一同丢进火炉里去。</p>    
    </body>
    </html>
    