
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="http://nautidea-10007657.cos.myqcloud.com/nautidea.dailyread.logo.512.png">
    <title>《福芝芳：经典“旺夫相”》李筱懿, 来自日读，每日经典推送，珍贵时光我们与你相伴</title>
    <script>
    var _hmt = _hmt || [];
    (function() {
    var hm = document.createElement("script");
    hm.src = "//hm.baidu.com/hm.js?16f893f6ab4a9f06cd8ada9e8ccb5376";
    var s = document.getElementsByTagName("script")[0]; 
    s.parentNode.insertBefore(hm, s);
    })();
    </script>
    </head>
    <body bgcolor="#fdf5dc" style="line-height:200%;padding:20px;">
    <h3 id="福芝芳经典旺夫相">福芝芳：经典“旺夫相”</h3>



<h4 id="李筱懿">李筱懿</h4>

<hr>

<p>深明大义有旺夫相的女子楷模是初中课文《乐羊子妻》中乐羊子的老婆。</p>

<p>老公外出求学一年后回家，她跪着问回来的原因，老公说：“在外久了，想家啊！”她听了立马拿起剪刀走向织布机：“蚕丝根根积累才成布匹，现在剪断就会丢弃成功的机会，你求学半途而废和剪断这些布有啥不一样呢？”</p>

<p>乐羊子被这番话感动，回去重修学业，七年都没回家。</p>

<p>我常想，这个场景若是寻常女子会说什么呢？</p>

<p>多半会乐颠乐颠地娇嗔两句，守着老公过幸福的小日子吧，所以大多数女人的老公也只能达到“老婆孩子热炕头”的境界。若想兼济天下成就事业，除了男人自身天赋异禀，娶个有“旺夫相”的老婆也很关键。</p>

<p>什么叫“旺夫相”呢？它至少包含三个层次内容：</p>

<p>第一，独立而耐得住寂寞，不给男人添麻烦，唧唧歪歪地粘人像林黛玉似的，老指着男人疼着、宠着、伺候着，男人哪有心思干大事啊？</p>

<p>第二，自觉地把丈夫的成功看得高于一切，急丈夫所急，想丈夫所想，永远是丈夫的拉拉队，遇到任何艰难险阻都要握紧拳头、鼓起腮帮坚定不移地说：“某某君，加油！”成功案例请参考希拉里克林顿。</p>

<p>第三，丈夫旺了之后依然能够守得住，吃得了苦，也享得了福，不能他发达之日就是你下岗之时，那再“旺夫”旺的也是别人的“夫”，也别像王宝钏似的，守了寒窑十八年，只过了十八天好日子就与世长辞了，那可真是命薄。</p>

<p>这样一过滤，茫茫天地间具备“旺夫相”的女子，福芝芳算一个。</p>

<p>梅兰芳的戏迷有个特别的称谓叫“梅党”，搁在现在要叫“团队”，这个团队做任何事情的出发点都是呵护梅兰芳，福、梅二人的姻缘就是梅兰芳团队重要成员冯六爷撮合的。</p>

<p>福芝芳是旗人，比梅兰芳小十一岁，从小文静内向不爱出门，总是和小花猫做伴，稍大点就和邻居的姑娘们一起在炕头上绣花，她那些当年一起玩的闺蜜后来都嫁了名伶，果素瑛嫁给了程砚秋，冯金芙则嫁给了开创京剧小生“姜派”的姜妙香。</p>

<p>她十四岁跟邻居吴菱仙学唱京剧，演出的《桑园会》《武家坡》《二进宫》颇受好评，被戏称“天桥梅兰芳”。父亲去世得早，外祖父离世后，她和母亲相依为命，困窘度日。母亲每天陪伴女儿上戏馆演出，为了安全，本来就身高马大的福母从此改着男装，再加上天生两肋插刀的侠义风范，福母被南城戏剧圈子称作“福二爷”。</p>

<p>德高望重的“梅党”要人冯六爷先去看过，对福芝芳有三条断言：“长得不错，唱得不错，能生孩子。”——这些“优点”正是当时的梅兰芳、梅家和“梅党”不可或缺的。</p>

<p>于是，1921年的一天，老师吴菱仙和京城名流罗瘿公受梅家之托到福芝芳家说媒。精明的福母知道，梅兰芳人品好、事业好，虽然已婚，但原配夫人王明华不能再生育，她果断谢绝了聘礼和定金，只提出两个条件：一是梅兰芳要按兼祧两房的规矩迎娶福芝芳，福芝芳与王明华同等名分，不能做妾；二是自己只有一个女儿，必须让她跟着女儿到梅家生活，将来梅兰芳要为她养老送终。</p>

<p>梅家上下和王明华答应了。</p>

<p>于是，福芝芳嫁给了梅兰芳，开始了四十年“扶植芳”的贤内助生涯。</p>

<p>新婚伊始，福芝芳面临不少挑战，比如，怎样提升自己的艺术品位，如何像王明华一样打理好梅兰芳的事业，怎么和原配夫人王明华相处，等等。</p>

<p>在母亲的指点下，福芝芳几乎交上了满分答卷。</p>

<p>她知道自己年幼学戏，文化欠缺，便请丈夫聘了位家庭教师，常年住在家中教习，每天上午读书识字，从不间断。</p>

<p>早上起床，先写一个时辰的毛笔字，然后从《三字经》《百家姓》《千字文》学起，之后是《唐诗三百首》《古文观止》，还会背《左传》。</p>

<p>除了古文，老师同时教她读白话文，阅读杂志。日积月累地学了四年多，她从起初的只能识读简单书信，到能看懂古文和白话文小说，再到可以陪着梅兰芳编排新戏、赏鉴绘画，进步神速。</p>

<p>所以，如此沉静、内秀、贤惠的女子，他会不爱吗？</p>

<p>除了家务和自修，她也常到剧场后台做些化妆、服装设计之类的工作。与王明华天生的艺术灵感不同，她情商很高，尤其善于化解矛盾，梅家班底演员之间的冲突和心事，总是她从中说和化解。</p>

<p>名角言慧珠1955年冬天试图自杀，抢救过来后福芝芳赶到医院看望，之后接她到梅宅调养，让她和自己的女儿同住一屋，犹如自己当年学戏时的模样。听言慧珠讲她的过往沉浮，悲欢倾诉，真心心疼这个“早生了一百年”的一代名伶。</p>

<p>尚小云被批斗、抄家，长久的颠沛流离之后在故人家里吃了一顿“炒面疙瘩”，一边狼吞虎咽地吃，一边说：“我这十几年也没有吃过这样的好饭菜了。”旁边的福芝芳笑着对他说：“留神点儿，可别吃得太撑啦。”</p>

<p>当年读章诒和的《伶人往事》，只见她怀念了八位戏曲界泰斗，尚小云、言慧珠、杨宝忠、叶盛兰、叶盛长、奚啸伯、马连良和程砚秋，还诧异其中居然没有梅兰芳。后来才明白，梅兰芳在梨园行里的地位，已然是卓然众生之上的“伶界大王”，他照顾疼惜着风雨飘摇的时代里落魄迷茫的同行，身后最有力的执行者，便是福芝芳。</p>

<p>1929年初，王明华在天津去世，按规矩应由她的子嗣将灵柩接回北京，而她膝下无儿女，福芝芳立即安排自己三岁的儿子梅葆琛作为王明华的孝子到天津去接灵柩，孩子太小，便让管家刘德君抱着打幡，务必尽全孝子的礼仪。</p>

<p>梅兰芳、福芝芳带着葆琪、葆琛和葆珍给王明华戴孝送葬，用金丝楠木棺材装殓，葬入万花山墓地，这块墓地修了三座墓，留给这段婚姻中和谐的三个人。福芝芳对王明华，自始至终算是有情有义了，发自内心地认可并维护三人行的婚姻，这份聪明、耐心和大器，绝大多数女子都望尘莫及。</p>

<p>福芝芳不仅得到梅兰芳的爱情，十四年里生了九个孩子，也得到了“梅党”团队的认可，顺风顺水的婚姻生活一直持续到孟小冬出现。</p>

<p>那是两人婚后第五年，梅兰芳在东城东四牌楼九条35号冯公馆缀玉轩娶了孟小冬，福芝芳伤心欲绝，不愿孟小冬进门，更不愿承认她。</p>

<p>如果说她对王明华是超越常人的义气，对孟小冬，则把普通女子遭遇爱情第三者的愤恨、嫉妒、决绝、城府表现得淋漓尽致。</p>

<p>人生，果真是个多面体。</p>

<p>为了与孟小冬争夺跟随梅兰芳去美国演出，在全世界面前以“梅夫人”的身份亮相，她不惜请医生给自己堕胎，两难的梅兰芳只好谁也没带，独自去了美国。</p>

<p>两人的斗争还扩散到梅兰芳伯母的丧礼上。孟小冬得信剪了短发，头插白花，来到梅宅，披麻戴孝。刚跨入大门，即被三四个下人拦住，大厅里的梅兰芳面露难色，望着怀胎已快足月、镇静地坐在灵堂恭迎客人的福芝芳，说：“不看僧面看佛面，小冬已经来了，我看就让她磕个头算了！”福芝芳刷地站了起来，厉声说：“这个门，她就是不能进！否则，我拿两个孩子、还有肚里一个，和她拼了！”</p>

<p>一场艰苦卓绝的婚姻保卫战，就此打响。</p>

<p>两个水火不容的女人，给梅兰芳出了道难题，只能二选一，究竟留谁？</p>

<p>从表面上看，福芝芳的全职太太职业生涯没有太大亮点。</p>

<p>论容貌，她肯定比不上更年轻、更美丽的孟小冬；论才华，孟小冬的唱腔被赞“前无古人”；论名气，“冬皇”名满京城声震上海；论感情，多年夫妻虽然恩爱，浓烈程度却敌不过激情燃烧的时刻；论后援，孟小冬的粉丝团和好事的记者们早就期待着“伶界大王”与“须生之皇”的珠联璧合。</p>

<p>如此强劲的对手，她胜券几多？</p>

<p>往事不用再提，人生已多风雨。她再次老辣而又历练地交上满分答卷，也为饱受婚外竞争者之苦的老婆们，提供了智取老公的范本。</p>

<p>首先，立场坚定毫不动摇。她旗帜鲜明地反对梅、孟结合，表示孟小冬要进梅家门不过是场“美梦”，毫无商量余地，两人之间只能二选一。</p>

<p>其次，打好“亲子牌”。对于梅兰芳这样儿女心重的男人，孩子是最牵肠挂肚的牵绊，自己当时三子傍身，孟小冬还是有花无果。</p>

<p>第三，矢志不渝地发动持久战。再深厚的感情也抵不过平淡的流年，红玫瑰总有被时间涤荡成白玫瑰的时候，时间长了起跑线就一致了，美丽让男人停下，智慧才能让男人留下哦。</p>

<p>第四，找到团队，抱团作战。团结一切可以团结的人，寻求有分量的盟友，梅兰芳团队一致赞同“逐孟留福”，都觉得福芝芳能“服侍人”，孟小冬却需要“人服侍”，为了boss梅兰芳的幸福从长计议，还是福芝芳更合适。</p>

<p>第五，做好事件营销，找准命门，一举击溃敌人。孟小冬的铁杆粉丝袭击梅兰芳之后，福芝芳与梅党提出，与孟小冬结合影响了梅兰芳的安全和事业发展，对于一个有梦想、有追求的成功男人，什么比事业受阻更致命？</p>

<p>第六，抓住一切机会，展示自我优势。虽然反对梅、孟在一起，福芝芳可没有整天唠叨，大多时候她都是不动声色的，去美国演出前梅兰芳给孟小冬和福芝芳分别留了几万块钱，回来时孟小冬早已花光，福芝芳不仅把全家老小照顾得妥妥帖帖，还结余不少，持家能力高低立显。</p>

<p>第七，懂得示弱。虽然关键时刻福芝芳做事凌厉，但她一直以梅、孟情感中的受害者形象出现，梅兰芳心疼她的伤心，也一直迁就她，与孟小冬另住“缀玉轩”。</p>

<p>在这场艰苦卓绝的婚姻保卫战中，知己知彼才能百战不殆，扬长避短才能战无不胜，天时地利才能攻无不克。</p>

<p>旺夫相的福芝芳最终绝地反击，孟小冬避走他乡。</p>

<p>很多人以为嫁得好比干得好容易，果真如此吗？</p>

<p>旺夫也是一种素质，需要怀揣满满的爱心和耐心，经历漫漫的忍耐和妥协，走过情感的纠结和磕绊，不然嫁得好也守不住。</p>

<p>婚姻甚至遵循着“适者生存”的自然界法则，在这场幸存者的游戏中，需要不断地审视自己、认清他人、把握全局、随机应变，才有可能留到最后。</p>

<p>只不过，大多女人面临的问题不是怎么去“旺夫”，而是这个男人值不值得“旺”，“旺夫”与“旺自己”，究竟哪个成本更低、收获更大呢？</p>

<p>梅兰芳晚年发福，现成的羊毛衫裤总是紧绷绷穿着不舒服。福芝芳便亲手编织了粗毛线、细毛线、深色、浅色的毛衣、毛裤和毛背心。王府井百货大楼开张营业时，她一下买了十多斤深棕色的毛线，为丈夫和三个儿子各织了一件开衫毛衣。</p>

<p>而孟小冬，一直把梅兰芳的照片供在自己香港的寓所。</p>

<p>一个男人，是情愿看自己的照片被安放在案头呢，还是穿着温暖牌毛裤呢？</p>

<p>治愈你：</p>

<p>宁财神描述一个男人的爱情梦想：</p>

<p>少年时，想碰到一个聂小倩，拼了性命爱一场，天亮前带着她的魂魄远走他乡。青年时，想碰到一个白素贞，家大业大，要啥有啥，吃完软饭一抹嘴，还有人负责把她收进雷峰塔。中年时，想要一个田螺姑娘，温婉可人，红袖添香，半夜写累了，让她变回原形，加干辣椒、花椒、姜、蒜片，爆炒，淋入香油，起锅装盘。</p>

<p>我的神啊，如果你想和一个男人厮守一生，做他永远的贤妻，最好既有聂小倩的妩媚妖娆，兼具白素贞的超能量，还得有田螺姑娘的雷锋精神。</p>

<p>你，扛得住吗？</p>

<p>那些以为贤妻良母是项低技术含量工作的人，和福芝芳轮个岗试试？</p>    
    </body>
    </html>
    